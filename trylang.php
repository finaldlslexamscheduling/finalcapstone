<?php
session_start();
include_once 'dbconnect.php';

if(!isset($_SESSION['user']))
{
	header("Location: index.php");
}
$res=mysql_query("SELECT * FROM users WHERE user_id=".$_SESSION['user']);
$userRow=mysql_fetch_array($res);

 ?>

<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" href="assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>DLSL CESS</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

	<!--     Fonts and icons     -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

	<!-- CSS Files -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/material-kit.css" rel="stylesheet"/>
		<link href="assets/css/demo.css" rel="stylesheet" />

</head>

<body class="profile-page">
	<nav class="navbar navbar-transparent navbar-absolute">
    	<div class="container">
        	<!-- Brand and toggle get grouped for better mobile display -->
        	<div class="navbar-header">
        		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example">
            		<span class="sr-only">Toggle navigation</span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
        		</button>
        		<!-- <a class="navbar-brand" href="http://dlslcess.byethost24.com">DLSL CESS</a> -->
						<a href="http://dlslcess.byethost24.com">
						     <div class="logo-container">
						          <div class="logo">
						              <img src="assets/img/dlsl.jpg" alt="DLSL CESS Logo">
						          </div>
						          <div class="brand">
						              DLSL CESS
						          </div>
						      </div>
						</a>
        	</div>

        	<div class="collapse navbar-collapse" id="navigation-example">
        		<ul class="nav navbar-nav navbar-right">
					<li>
    					<a href="SecretaryHome.php">
    						Home
    					</a>
    				</li>
    				<li>
						<a href="#">
							Notification
						</a>
    				</li>
						<li>
						<a href="SecretaryProfile.php">
							Profile
						</a>
						</li>
						<li>
						<a href="SecretaryScheduleInformation.php">
							Schedule Information
						</a>
					</li>
					<li>
					<a href="SecretaryTeachingLoadInformation.php">
						Teaching Load Information
					</a>
				</li>
						<li>
						<a href="SecretarySpecialExamRequest.php">
							Special Exam Request
						</a>
						</li>
						<li>
						<a href="SecretaryManage.php">
							Manage
						</a>
						</li>
						<li>
						<a href="logout.php?logout">Log out</a>
						</a>
						</li>
        		</ul>
        	</div>
    	</div>
    </nav>

    <div class="wrapper">
		<div class="header header-filter" style="background-image: url('assets/img/examples/bground.jpg');"></div>

		<div class="main main-raised">
			<div class="profile-content">
	            <div class="container">
	                <div class="row">
	                    <div class="profile">
	                        <div class="avatar">
	                            <img src="assets/img/default-avatar.png" alt="Circle Image" class="img-circle img-responsive img-raised">
	                        </div>
	                        <div class="name">
	                            <h3 class="title">Welcome, Secretary</h3>
	                        </div>
	                    </div>
	                </div>

									<div class="row">
										<div class="col-md-6 col-md-offset-3">
											<div class="profile-tabs">

												<div class="table-responsive">

													<div class="form-group pull-right">
														<input type="text" class="search" placeholder="Search...">
												</div>
									 				<table class="table results">
									 								    <thead>
									 								        <tr>
									 								            <th style="text-align:center;" class="">Schedule ID</th>
									 								            <th style="text-align:center;" class="">Date</th>
									 								            <th style="text-align:center;" class="">Day</th>
									 								            <th style="text-align:center;" class="">Time ID</th>
									 								            <th style="text-align:center;" class="">Start</th>
																							<th style="text-align:center;" class="">End</th>
																							<th style="text-align:center;" class="">Units</th>
																							<th style="text-align:center;" class="">Subject Code</th>
																							<th style="text-align:center;" class="">Section</th>
																							<th style="text-align:center;" class="">Sub Units</th>
																							<th style="text-align:center;" class="">Room</th>
																							<th style="text-align:center;" class="">Room Type</th>
																							<th style="text-align:center;" class="">Room ID</th>
																							<th style="text-align:center;" class="">Proctor</th>
									 								        </tr>
																					<tr class="no-result">
																			      <td colspan="4"><center>No results to be displayed</center></td>
																			    </tr>
									 								    </thead>
									 								    <tbody>
									 								        <tr>
									 								          <?php
									 								          include_once 'dbconnect.php';
																						$college_name=$userRow['college_name'];

																							$result = mysql_query("SELECT * FROM try");
																							while($row = mysql_fetch_array($result))
																								{
																								echo "<tr>";
																								echo "<td>" . $row['sched_id'] . "</td>";
																								echo "<td>" . $row['date'] . "</td>";
																								echo "<td>" . $row['day'] . "</td>";
																								echo "<td>" . $row['time_id'] . "</td>";
																								echo "<td>" . $row['start'] . "</td>";
																								echo "<td>" . $row['end'] . "</td>";
																								echo "<td>" . $row['units'] . "</td>";
																								echo "<td>" . $row['subject_code'] . "</td>";
																								echo "<td>" . $row['section'] . "</td>";
																								echo "<td>" . $row['sub_units'] . "</td>";
																								echo "<td>" . $row['room'] . "</td>";
																								echo "<td>" . $row['room_type'] . "</td>";
																								echo "<td>" . $row['room_id'] . "</td>";
																								echo "<td>" . $row['proctor'] . "</td>";
																								}
									 								          ?>
																					</tr>
																				</tbody>
																			</table>
																		</div>
																		<!-- End Profile Tabs -->
																	</div>
																</div>

																		</div>
																</div>
															</div>
													</div>

    <footer class="footer">
        <div class="container">
            <div class="copyright">
                <center>&copy; 2017 DLSL CESS</center>
            </div>
        </div>
    </footer>

</body>

	<!--   Core JS Files   -->
	<script src="assets/js/jquery.min.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/js/material.min.js"></script>

	<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
	<script src="assets/js/nouislider.min.js" type="text/javascript"></script>

	<!--  Plugin for the Datepicker, full documentation here: http://www.eyecon.ro/bootstrap-datepicker/ -->
	<script src="assets/js/bootstrap-datepicker.js" type="text/javascript"></script>

	<!-- Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc -->
	<script src="assets/js/material-kit.js" type="text/javascript"></script>

</html>
