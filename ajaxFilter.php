<?php
	include "pdo.php";
	session_start();

	// if($_POST["type"]=="all")
	// 	$stmt = $dbh->prepare("SELECT * FROM exam_schedule");
	if($_POST["type"]=="day")
		$stmt = $dbh->prepare("SELECT DISTINCT day FROM exam_schedule");
	if($_POST["type"]=="date")
		$stmt = $dbh->prepare("SELECT DISTINCT date FROM exam_schedule");
	if($_POST["type"]=="starttime")
		$stmt = $dbh->prepare("SELECT DISTINCT starttime FROM exam_schedule");
	if($_POST["type"]=="endtime")
		$stmt = $dbh->prepare("SELECT DISTINCT endtime FROM exam_schedule");
	if($_POST["type"]=="room")
		$stmt = $dbh->prepare("SELECT DISTINCT room FROM exam_schedule");
	if($_POST["type"]=="room_type")
		$stmt = $dbh->prepare("SELECT DISTINCT room_type FROM exam_schedule");
	if($_POST["type"]=="subject_code")
		$stmt = $dbh->prepare("SELECT DISTINCT subject_code FROM exam_schedule");
	if($_POST["type"]=="section")
		$stmt = $dbh->prepare("SELECT DISTINCT section FROM exam_schedule");
	if($_POST["type"]=="proctor")
		$stmt = $dbh->prepare("SELECT DISTINCT proctor FROM exam_schedule");
	if($_POST["type"]=="college_name")
		$stmt = $dbh->prepare("SELECT DISTINCT college_name FROM exam_schedule");
	if($_POST["type"]=="noproctor")
		$stmt = $dbh->prepare("SELECT DISTINCT section FROM exam_schedule where taken = 1 and proctortaken = 0");
	if($_POST["type"]=="availableroom")
		$stmt = $dbh->prepare("SELECT DISTINCT room FROM exam_schedule WHERE taken = 0");
	$stmt->execute();
	$filters = $stmt->fetchAll();

	echo json_encode($filters);

?>