-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 04, 2017 at 01:06 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dlslces`
--

-- --------------------------------------------------------

--
-- Table structure for table `college`
--

CREATE TABLE `college` (
  `col_id` varchar(60) NOT NULL,
  `college_name` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `college`
--

INSERT INTO `college` (`col_id`, `college_name`) VALUES
('1', 'CITE'),
('2', 'CBEAM'),
('3', 'CEAS'),
('4', 'CITHM'),
('5', 'CON');

-- --------------------------------------------------------

--
-- Table structure for table `exam_schedule`
--

CREATE TABLE `exam_schedule` (
  `date` varchar(255) NOT NULL,
  `day` varchar(255) NOT NULL,
  `start` varchar(255) NOT NULL,
  `end` varchar(255) NOT NULL,
  `subject_code` varchar(255) NOT NULL,
  `section` varchar(255) NOT NULL,
  `room` varchar(255) NOT NULL,
  `proctor` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `proctor_cbeam`
--

CREATE TABLE `proctor_cbeam` (
  `availability` varchar(60) NOT NULL,
  `start` varchar(60) NOT NULL,
  `end` varchar(60) NOT NULL,
  `proctor` varchar(60) NOT NULL,
  `proctortype` varchar(60) NOT NULL,
  `user_type` varchar(255) NOT NULL,
  `collage_id` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `proctor_cbeam`
--

INSERT INTO `proctor_cbeam` (`availability`, `start`, `end`, `proctor`, `proctortype`, `user_type`, `collage_id`) VALUES
('TTH', '7:50 AM', '10:50 AM', 'Abu, Janine CPA', 'PT', 'faculty', '2'),
('TTH', '10:55 AM', '12:25 PM', 'Abu, Janine CPA', 'PT', 'faculty', '2'),
('TTH', '1:00 PM', '2:30 PM', 'Abu, Janine CPA', 'PT', 'faculty', '2'),
('TTH', '4:00 PM', '7:00 PM', 'Abu, Janine CPA', 'PT', 'faculty', '2'),
('MWF', '8:35 AM', '9:35 AM', 'Agonia, Baby Maricel G., MPopS', 'FT', 'faculty', '2'),
('MWF', '9:55 AM', '10:55 AM', 'Agonia, Baby Maricel G., MPopS', 'FT', 'faculty', '2'),
('MWF', '1:10 PM', '2:10 PM', 'Agonia, Baby Maricel G., MPopS', 'FT', 'faculty', '2'),
('MWF', '3:35 PM', '4:35 PM', 'Agonia, Baby Maricel G., MPopS', 'FT', 'faculty', '2'),
('TTH', '1:00 PM', '2:30 PM', 'Agonia, Baby Maricel G., MPopS', 'FT', 'faculty', '2'),
('TTH', '3:55 PM', '5:25 PM', 'Agonia, Baby Maricel G., MPopS', 'FT', 'faculty', '2'),
('MWF', '12:05 PM', '1:05 PM', 'Altar, Elsa Glinda G., CPA, MAEd', 'PT', 'faculty', '2'),
('MWF', '1:10 PM', '2:10 PM', 'Altar, Elsa Glinda G., CPA, MAEd', 'PT', 'faculty', '2'),
('MWF', '2:15 PM', '3:15 PM', 'Altar, Elsa Glinda G., CPA, MAEd', 'PT', 'faculty', '2'),
('MWF', '3:35 PM', '4:35 PM', 'Altar, Elsa Glinda G., CPA, MAEd', 'PT', 'faculty', '2'),
('TTH', '9:20 AM', '10:50 AM', 'Altar, Elsa Glinda G., CPA, MAEd', 'PT', 'faculty', '2'),
('MWF', '1:10 PM', '2:10 PM', 'Bagnes, Bernice L., MMT', 'PT', 'faculty', '2'),
('MWF', '2:15 PM', '3:15 PM', 'Bagnes, Bernice L., MMT', 'PT', 'faculty', '2'),
('MWF', '3:35 PM', '4:35 PM', 'Bagnes, Bernice L., MMT', 'PT', 'faculty', '2'),
('TTH', '9:20 AM', '10:50 AM', 'Bagnes, Bernice L., MMT', 'PT', 'faculty', '2'),
('TTH', '10:55 AM', '12:25 PM', 'Bagnes, Bernice L., MMT', 'PT', 'faculty', '2'),
('TTH', '1:00 PM', '2:30 PM', 'Bagnes, Bernice L., MMT', 'PT', 'faculty', '2'),
('S', '11:00 AM', '2:00 PM', 'Bagobagon, Ma. Montserrat R., CPA', 'PT', 'faculty', '2'),
('S', '2:30 PM', '5:30 PM', 'Bagobagon, Ma. Montserrat R., CPA', 'PT', 'faculty', '2'),
('MWF', '4:40 PM', '5:40 PM', 'Buenafe, Marjeric L., MBA', 'PT', 'faculty', '2'),
('T', '5:30 PM', '8:30 PM', 'Caintic, Ma. Angelica M., MPA', 'PT', 'faculty', '2'),
('TH', '5:30 PM', '8:30 PM', 'Caintic, Ma. Angelica M., MPA', 'PT', 'faculty', '2'),
('MWF', '7:30 AM', '9:30 AM', 'Caguitla, Jebong M., CPA', 'PT', 'faculty', '2'),
('MWF', '10:00 AM', '12:00 PM', 'Caguitla, Jebong M., CPA', 'PT', 'faculty', '2'),
('S', '7:30 AM', '10:30 AM', 'Carpio, Romelyn M., CPA', 'PT', 'faculty', '2'),
('S', '11:00 AM', '2:00 PM', 'Carpio, Romelyn M., CPA', 'PT', 'faculty', '2'),
('S', '2:30 PM', '5:30 PM', 'Carpio, Romelyn M., CPA', 'PT', 'faculty', '2'),
('MWF', '9:55 AM', '10:55 AM', 'Castillo, Amelito M., MMT', 'FT', 'faculty', '2'),
('MWF', '11:00 AM', '12:00 PM', 'Castillo, Amelito M., MMT', 'FT', 'faculty', '2'),
('MWF', '1:10 PM', '2:10 PM', 'Castillo, Amelito M., MMT', 'FT', 'faculty', '2'),
('MWF', '2:15 PM', '3:15 PM', 'Castillo, Amelito M., MMT', 'FT', 'faculty', '2'),
('MWF', '4:40 PM', '5:40 PM', 'Castillo, Amelito M., MMT', 'FT', 'faculty', '2'),
('TTH', '5:30 PM', '7:00 PM', 'Castillo, Amelito M., MMT', 'FT', 'faculty', '2'),
('TTH', '7:05 PM', '8:35 PM', 'Castillo, Amelito M., MMT', 'FT', 'faculty', '2'),
('S', '2:30 PM', '5:30 PM', 'Castillo, Amelito M., MMT', 'FT', 'faculty', '2'),
('M', '5:30 PM', '8:30 PM', 'Castillo, Atty. Triven P.', 'PT', 'faculty', '2'),
('MWF', '3:35 PM', '4:35 PM', 'Castro, Edgar Allan G., MMT, Ph.D', 'PT', 'faculty', '2'),
('TTH', '7:05 PM', '8:35 PM', 'Castro, Edgar Allan G., MMT, Ph.D', 'PT', 'faculty', '2'),
('TTH', '3:55 PM', '5:25 PM', 'Castro, Edgar Allan G., MMT, Ph.D', 'PT', 'faculty', '2'),
('TTH', '5:30 PM', '7:00 PM', 'Castro, Edgar Allan G., MMT, Ph.D', 'PT', 'faculty', '2'),
('S', '2:30 PM', '5:30 PM', 'Castro, Edgar Allan G., MMT, Ph.D', 'PT', 'faculty', '2'),
('S', '7:30 AM', '10:30 AM', 'Cometa, Anthony Joseph D., CPA', 'PT', 'faculty', '2'),
('S', '11:00 AM', '2:00 PM', 'Cometa, Anthony Joseph D., CPA', 'PT', 'faculty', '2'),
('S', '2:30 PM', '5:30 PM', 'Cometa, Anthony Joseph D., CPA', 'PT', 'faculty', '2'),
('MWF', '7:30 AM', '9:30 AM', 'Cortez, Antonio A., CPA', 'PT', 'faculty', '2'),
('MWF', '10:00 AM', '12:00 PM', 'Cortez, Antonio A., CPA', 'PT', 'faculty', '2'),
('MWF', '12:05 PM', '1:05 PM', 'Cortez, Antonio A., CPA', 'PT', 'faculty', '2'),
('MWF', '2:15 PM', '3:15 PM', 'Cortez, Antonio A., CPA', 'PT', 'faculty', '2'),
('MWF', '5:45 PM', '6:45 PM', 'Cortez, Antonio A., CPA', 'PT', 'faculty', '2'),
('S', '11:00 AM', '2:00 PM', 'Dalawampu, Atty. Loiue Mark M.', 'PT', 'faculty', '2'),
('S', '2:30 PM', '5:30 PM', 'Dalawampu, Atty. Loiue Mark M.', 'PT', 'faculty', '2'),
('MWF', '9:55 AM', '10:55 AM', 'De Silva, Jasmin D., MBA', 'FT', 'faculty', '2'),
('MWF', '11:00 AM', '12:00 PM', 'De Silva, Jasmin D., MBA', 'FT', 'faculty', '2'),
('MWF', '2:15 PM', '3:15 PM', 'De Silva, Jasmin D., MBA', 'FT', 'faculty', '2'),
('TTH', '3:55 PM', '5:25 PM', 'De Silva, Jasmin D., MBA', 'FT', 'faculty', '2'),
('TTH', '1:00 PM', '2:30 PM', 'De Silva, Jasmin D., MBA', 'FT', 'faculty', '2'),
('MWF', '6:50 PM', '7:50 PM', 'De Silva, Jasmin D., MBA', 'FT', 'faculty', '2'),
('MWF', '7:30 AM', '8:30 AM', 'De Silva, Jasmin D., MBA', 'FT', 'faculty', '2'),
('MWF', '1:10 PM', '2:10 PM', 'De Silva, Jasmin D., MBA', 'FT', 'faculty', '2'),
('TTH', '1:00 PM', '2:30 PM', 'Delica, Marcielo E., MBA', 'FT', 'faculty', '2'),
('MWF', '11:00 AM', '12:00 PM', 'Delica, Marcielo E., MBA', 'FT', 'faculty', '2'),
('MWF', '7:30 AM', '8:30 AM', 'Delica, Marcielo E., MBA', 'FT', 'faculty', '2'),
('MWF', '2:15 PM', '3:15 PM', 'Delica, Marcielo E., MBA', 'FT', 'faculty', '2'),
('MWF', '1:10 PM', '2:10 PM', 'Delica, Marcielo E., MBA', 'FT', 'faculty', '2'),
('TTH', '3:55 PM', '5:25 PM', 'Delica, Marcielo E., MBA', 'FT', 'faculty', '2'),
('S', '7:30 AM', '10:30 AM', 'Dimaano, Dr. Wilfreda D.', 'FT', 'faculty', '2'),
('S', '7:30 AM', '1:30 PM', 'Dimaano, Dr. Wilfreda D.', 'FT', 'faculty', '2'),
('S', '2:30 PM', '5:30 PM', 'Dimaano, Dr. Wilfreda D.', 'FT', 'faculty', '2'),
('MWF', '3:35 PM', '4:35 PM', 'Dimaano, Dr. Wilfreda D.', 'FT', 'faculty', '2'),
('MWF', '8:35 AM', '9:35 AM', 'Dimaano, Dr. Wilfreda D.', 'FT', 'faculty', '2'),
('MWF', '2:15 PM', '3:15 PM', 'Dimaano, Dr. Wilfreda D.', 'FT', 'faculty', '2'),
('MWF', '1:10 PM', '2:10 PM', 'Dimaano, Dr. Wilfreda D.', 'FT', 'faculty', '2'),
('MWF', '7:30 AM', '8:30 AM', 'Dimaano, Dr. Wilfreda D.', 'FT', 'faculty', '2'),
('TTH', '1:00 PM', '2:30 PM', 'Dimayacyac, Marites R., MMT', 'PT', 'faculty', '2'),
('MWF', '1:10 PM', '2:10 PM', 'Dimayacyac, Marites R., MMT', 'PT', 'faculty', '2'),
('MWF', '9:55 AM', '10:55 AM', 'Dimayacyac, Marites R., MMT', 'PT', 'faculty', '2'),
('TTH', '10:55 AM', '12:25 PM', 'Dimayacyac, Marites R., MMT', 'PT', 'faculty', '2'),
('TTH', '3:55 PM', '5:25 PM', 'Dimayacyac, Marites R., MMT', 'PT', 'faculty', '2'),
('MWF', '3:35 PM', '4:35 PM', 'Dimayacyac, Marites R., MMT', 'PT', 'faculty', '2'),
('S', '2:30 PM', '5:30 PM', 'Evangelista, Susie Jean R., CPA', 'PT', 'faculty', '2'),
('S', '11:00 AM', '2:00 PM', 'Evangelista, Susie Jean R., CPA', 'PT', 'faculty', '2'),
('MWF', '3:35 PM', '4:35 PM', 'Fampo, Jaylen C., MMT', 'FT', 'faculty', '2'),
('MWF', '9:55 AM', '10:55 AM', 'Fampo, Jaylen C., MMT', 'FT', 'faculty', '2'),
('MWF', '4:40 PM', '5:40 PM', 'Fampo, Jaylen C., MMT', 'FT', 'faculty', '2'),
('MWF', '8:35 AM', '9:35 AM', 'Fampo, Jaylen C., MMT', 'FT', 'faculty', '2'),
('TTH', '10:55 AM', '12:25 PM', 'Fampo, Jaylen C., MMT', 'FT', 'faculty', '2'),
('TTH', '9:20 AM', '10:50 AM', 'Fampo, Jaylen C., MMT', 'FT', 'faculty', '2'),
('TTH', '5:30 PM', '7:00 PM', 'Garachico, Lani R., CPA, MBA', 'FT', 'faculty', '2'),
('TTH', '7:05 PM', '8:35 PM', 'Garachico, Lani R., CPA, MBA', 'FT', 'faculty', '2'),
('MWF', '3:35 PM', '4:35 PM', 'Garachico, Lani R., CPA, MBA', 'FT', 'faculty', '2'),
('TTH', '9:20 AM', '10:50 AM', 'Gatus, Teodora G., MBA', 'FT', 'faculty', '2'),
('TTH', '10:55 AM', '12:25 PM', 'Gatus, Teodora G., MBA', 'FT', 'faculty', '2'),
('TTH', '3:55 PM', '5:25 PM', 'Gatus, Teodora G., MBA', 'FT', 'faculty', '2'),
('MWF', '8:35 AM', '9:35 AM', 'Gatus, Teodora G., MBA', 'FT', 'faculty', '2'),
('MWF', '9:55 AM', '10:55 AM', 'Gatus, Teodora G., MBA', 'FT', 'faculty', '2'),
('MWF', '11:00 AM', '12:00 PM', 'Gatus, Teodora G., MBA', 'FT', 'faculty', '2'),
('MWF', '2:15 PM', '3:15 PM', 'Gatus, Teodora G., MBA', 'FT', 'faculty', '2'),
('TTH', '5:30 PM', '7:00 PM', 'Gozos, Exur John Thomas M., MPA', 'PT', 'faculty', '2'),
('TTH', '7:05 PM', '8:35 PM', 'Gozos, Exur John Thomas M., MPA', 'PT', 'faculty', '2'),
('S', '7:30 AM', '10:30 AM', 'Gozos, Exur John Thomas M., MPA', 'PT', 'faculty', '2'),
('S', '11:00 AM', '2:00 PM', 'Ilagan, Judge Elizabeth, LL.M.', 'PT', 'faculty', '2'),
('S', '2:30 PM', '5:30 PM', 'Ilagan, Judge Elizabeth, LL.M.', 'PT', 'faculty', '2'),
('S', '11:00 AM', '2:00 PM', 'Javier, Jenica N., CPA', 'PT', 'faculty', '2'),
('S', '7:30 AM', '10:30 AM', 'Javier, Jenica N., CPA', 'PT', 'faculty', '2'),
('TTH', '5:30 PM', '7:00 PM', 'Javier, Jenica N., CPA', 'PT', 'faculty', '2'),
('TTH', '7:05 PM', '8:35 PM', 'Javier, Jenica N., CPA', 'PT', 'faculty', '2'),
('TTH', '5:30 PM', '7:00 PM', 'Kasilag, Michael H., MMT', 'PT', 'faculty', '2'),
('MWF', '4:40 PM', '5:40 PM', 'Kasilag, Michael H., MMT', 'PT', 'faculty', '2'),
('S', '11:00 AM', '2:00 PM', 'Laco, Atty. Reginald L., CPA', 'PT', 'faculty', '2'),
('S', '7:30 AM', '10:30 AM', 'Laco, Atty. Reginald L., CPA', 'PT', 'faculty', '2'),
('MWF', '2:15 PM', '3:15 PM', 'Lanip, Gerard J.', 'PT', 'faculty', '2'),
('MWF', '1:10 PM', '2:10 PM', 'Lanip, Gerard J.', 'PT', 'faculty', '2'),
('MWF', '11:00 AM', '12:00 PM', 'Lanip, Gerard J.', 'PT', 'faculty', '2'),
('MWF', '3:35 PM', '4:35 PM', 'Lanip, Gerard J.', 'PT', 'faculty', '2'),
('TTH', '3:55 PM', '5:25 PM', 'Lanip, Gerard J.', 'PT', 'faculty', '2'),
('MWF', '3:35 PM', '4:35 PM', 'Linatoc, Teresita C., RND, MA, MBA', 'FT', 'faculty', '2'),
('MWF', '11:00 AM', '12:00 PM', 'Linatoc, Teresita C., RND, MA, MBA', 'FT', 'faculty', '2'),
('MWF', '7:30 AM', '8:30 AM', 'Linatoc, Teresita C., RND, MA, MBA', 'FT', 'faculty', '2'),
('TTH', '9:20 AM', '10:50 AM', 'Linatoc, Teresita C., RND, MA, MBA', 'FT', 'faculty', '2'),
('MWF', '8:35 AM', '9:35 AM', 'Linatoc, Teresita C., RND, MA, MBA', 'FT', 'faculty', '2'),
('TTH', '7:30 AM', '9:00 AM', 'Linatoc, Teresita C., RND, MA, MBA', 'FT', 'faculty', '2'),
('S', '7:30 AM', '1:30 PM', 'Lucasia, Nerissa O., MMT', 'FT', 'faculty', '2'),
('MWF', '8:35 AM', '9:35 AM', 'Lucasia, Nerissa O., MMT', 'FT', 'faculty', '2'),
('MWF', '1:10 PM', '2:10 PM', 'Lucasia, Nerissa O., MMT', 'FT', 'faculty', '2'),
('MWF', '9:55 AM', '10:55 AM', 'Lucasia, Nerissa O., MMT', 'FT', 'faculty', '2'),
('MWF', '7:30 AM', '9:30 AM', 'Macalintal, Armer E., CPA', 'PT', 'faculty', '2'),
('MWF', '9:55 AM', '10:55 AM', 'Macalintal, Armer E., CPA', 'PT', 'faculty', '2'),
('MWF', '1:10 PM', '3:10 PM', 'Macalintal, Armer E., CPA', 'PT', 'faculty', '2'),
('TTH', '10:55 AM', '12:25 PM', 'Maduramente, Rodolfo R., MPA', 'FT', 'faculty', '2'),
('MWF', '1:10 PM', '2:10 PM', 'Maduramente, Rodolfo R., MPA', 'FT', 'faculty', '2'),
('TTH', '3:55 PM', '5:25 PM', 'Maduramente, Rodolfo R., MPA', 'FT', 'faculty', '2'),
('MWF', '11:00 AM', '12:00 PM', 'Maduramente, Rodolfo R., MPA', 'FT', 'faculty', '2'),
('MWF', '9:55 AM', '10:55 AM', 'Maduramente, Rodolfo R., MPA', 'FT', 'faculty', '2'),
('TTH', '9:20 AM', '10:50 AM', 'Maduramente, Rodolfo R., MPA', 'FT', 'faculty', '2'),
('TTH', '10:55 AM', '12:25 PM', 'Magsino, Maria Theresa M., MBA', 'FT', 'faculty', '2'),
('MWF', '3:35 PM', '4:35 PM', 'Magsino, Maria Theresa M., MBA', 'FT', 'faculty', '2'),
('TTH', '7:30 AM', '9:00 AM', 'Magsino, Maria Theresa M., MBA', 'FT', 'faculty', '2'),
('MWF', '11:00 AM', '12:00 PM', 'Magsino, Maria Theresa M., MBA', 'FT', 'faculty', '2'),
('S', '11:00 AM', '2:00 PM', 'Magsino, Maria Theresa M., MBA', 'FT', 'faculty', '2'),
('M', '5:30 PM', '8:30 PM', 'Malaluan, Atty. Krissandra', 'PT', 'faculty', '2'),
('TTH', '7:05 PM', '8:35 PM', 'Malaluan, Atty. Krissandra', 'PT', 'faculty', '2'),
('S', '7:30 AM', '10:30 AM', 'Malaluan, Atty. Krissandra', 'PT', 'faculty', '2'),
('TTH', '10:55 AM', '12:55 PM', 'Maloles, Sheila E.', 'FT', 'faculty', '2'),
('TTH', '9:20 AM', '10:50 AM', 'Maloles, Sheila E.', 'FT', 'faculty', '2'),
('TTH', '1:00 PM', '2:30 PM', 'Maloles, Sheila E.', 'FT', 'faculty', '2'),
('S', '2:30 PM', '5:30 PM', 'Maloles, Sheila E.', 'FT', 'faculty', '2'),
('MWF', '7:30 AM', '8:30 AM', 'Maloles, Sheila E.', 'FT', 'faculty', '2'),
('S', '11:00 AM', '2:00 PM', 'Manaig, Atty. Gileen C.', 'PT', 'faculty', '2'),
('S', '7:30 AM', '10:30 AM', 'Manaig, Atty. Gileen C.', 'PT', 'faculty', '2'),
('S', '2:30 PM', '5:30 PM', 'Mantuano, Arvin Jay, CPA', 'PT', 'faculty', '2'),
('S', '11:00 AM', '2:00 PM', 'Mantuano, Arvin Jay, CPA', 'PT', 'faculty', '2'),
('S', '7:30 AM', '10:30 AM', 'Mantuano, Arvin Jay, CPA', 'PT', 'faculty', '2'),
('S', '11:00 AM', '2:00 PM', 'Marcelo, Kathleen, CPA', 'PT', 'faculty', '2'),
('S', '2:30 PM', '5:30 PM', 'Marcelo, Kathleen, CPA', 'PT', 'faculty', '2'),
('S', '7:30 AM', '10:30 AM', 'Marcelo, Kathleen, CPA', 'PT', 'faculty', '2'),
('M', '1:00 PM', '3:00 PM', 'Matencio, Areanne G.', 'PT', 'faculty', '2'),
('F', '1:00 PM', '2:00 PM', 'Matencio, Areanne G.', 'PT', 'faculty', '2'),
('T', '5:30 PM', '8:30 PM', 'Matencio, Areanne G.', 'PT', 'faculty', '2'),
('TH', '5:30 PM', '8:30 PM', 'Matencio, Areanne G.', 'PT', 'faculty', '2'),
('W', '5:30 PM', '8:30 PM', 'Matuloy, Atty. Rhandelle Alvin', 'PT', 'faculty', '2'),
('TTH', '7:30 AM', '9:00 AM', 'Mendoza, Eden Loraine T., CPA', 'PT', 'faculty', '2'),
('MWF', '4:40 PM', '6:40 PM', 'Mendoza, Eden Loraine T., CPA', 'PT', 'faculty', '2'),
('TTH', '5:30 PM', '7:00 PM', 'Mendoza, Eden Loraine T., CPA', 'PT', 'faculty', '2'),
('TTH', '3:55 PM', '5:25 PM', 'Mendoza, Eden Loraine T., CPA', 'PT', 'faculty', '2'),
('TTH', '1:00 PM', '2:30 PM', 'Mendoza, Eden Loraine T., CPA', 'PT', 'faculty', '2'),
('S', '2:30 PM', '5:30 PM', 'Meneses, Atty. Marylou P.', 'PT', 'faculty', '2'),
('S', '11:00 AM', '2:00 PM', 'Meneses, Atty. Marylou P.', 'PT', 'faculty', '2'),
('T', '5:30 PM', '8:30 PM', 'Mercado, Ferdinand G., MMT', 'PT', 'faculty', '2'),
('MWF', '4:40 PM', '6:40 PM', 'Mojado, Ronald D., CPA', 'PT', 'faculty', '2'),
('MWF', '2:15 PM', '4:15 PM', 'Mojado, Ronald D., CPA', 'PT', 'faculty', '2'),
('MWF', '8:35 AM', '10:35 AM', 'Mores, Renillyn F., CPA, MBA', 'FT', 'faculty', '2'),
('MWF', '2:15 PM', '4:15 PM', 'Mores, Renillyn F., CPA, MBA', 'FT', 'faculty', '2'),
('TTH', '3:55 PM', '5:25 PM', 'Mores, Renillyn F., CPA, MBA', 'FT', 'faculty', '2'),
('TTH', '1:00 PM', '2:30 PM', 'Mores, Renillyn F., CPA, MBA', 'FT', 'faculty', '2'),
('TTH', '10:55 AM', '12:25 PM', 'Mores, Renillyn F., CPA, MBA', 'FT', 'faculty', '2'),
('F', '9:00 AM', '12:00 PM', 'Muria, Atty. Ramel C., LL.M.', 'PT', 'faculty', '2'),
('F', '1:00 PM', '4:00 PM', 'Muria, Atty. Ramel C., LL.M.', 'PT', 'faculty', '2'),
('S', '7:30 AM', '1:30 PM', 'Osorio, Joel M., MMT', 'PT', 'faculty', '2'),
('TTH', '7:30 AM', '9:00 AM', 'Osorio, Joel M., MMT', 'PT', 'faculty', '2'),
('TTH', '3:55 PM', '5:25 PM', 'Osorio, Joel M., MMT', 'PT', 'faculty', '2'),
('TTH', '1:00 PM', '2:30 PM', 'Osorio, Joel M., MMT', 'PT', 'faculty', '2'),
('TTH', '9:20 AM', '10:50 AM', 'Osorio, Joel M., MMT', 'PT', 'faculty', '2'),
('S', '7:30 AM', '1:30 PM', 'Pagsuyuin, Alan C., MBA', 'FT', 'faculty', '2'),
('S', '7:30 AM', '1:30 PM', 'Pagsuyuin, Alan C., MBA', 'FT', 'faculty', '2'),
('TTH', '10:55 AM', '12:25 PM', 'Pagsuyuin, Alan C., MBA', 'FT', 'faculty', '2'),
('TTH', '1:00 PM', '2:30 PM', 'Pagsuyuin, Alan C., MBA', 'FT', 'faculty', '2'),
('TTH', '9:20 AM', '10:50 AM', 'Pagsuyuin, Alan C., MBA', 'FT', 'faculty', '2');

-- --------------------------------------------------------

--
-- Table structure for table `proctor_ceas`
--

CREATE TABLE `proctor_ceas` (
  `availability` varchar(60) NOT NULL,
  `start` varchar(60) NOT NULL,
  `end` varchar(60) NOT NULL,
  `proctor` varchar(60) NOT NULL,
  `proctortype` varchar(60) NOT NULL,
  `user_type` varchar(255) NOT NULL,
  `collage_id` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `proctor_cite`
--

CREATE TABLE `proctor_cite` (
  `availability` varchar(60) NOT NULL,
  `start` varchar(60) NOT NULL,
  `end` varchar(60) NOT NULL,
  `proctor` varchar(60) NOT NULL,
  `proctortype` varchar(60) NOT NULL,
  `user_type` varchar(255) NOT NULL,
  `collage_id` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `proctor_cithm`
--

CREATE TABLE `proctor_cithm` (
  `availability` varchar(60) NOT NULL,
  `start` varchar(60) NOT NULL,
  `end` varchar(60) NOT NULL,
  `proctor` varchar(60) NOT NULL,
  `proctortype` varchar(60) NOT NULL,
  `user_type` varchar(255) NOT NULL,
  `collage_id` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `proctor_con`
--

CREATE TABLE `proctor_con` (
  `availability` varchar(60) NOT NULL,
  `start` varchar(60) NOT NULL,
  `end` varchar(60) NOT NULL,
  `proctor` varchar(60) NOT NULL,
  `proctortype` varchar(60) NOT NULL,
  `user_type` varchar(255) NOT NULL,
  `collage_id` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `room_name` varchar(60) NOT NULL,
  `room_type` varchar(60) NOT NULL,
  `capacity` varchar(60) NOT NULL,
  `collage_id` varchar(60) NOT NULL,
  `idd` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`room_name`, `room_type`, `capacity`, `collage_id`, `idd`) VALUES
('CB 205', 'Lec', '40', '2', '1'),
('CB 203', 'Lec', '40', '2', '2'),
('CB 204', 'Lec', '40', '2', '3'),
('CB 308', 'Lec', '40', '2', '4'),
('CB 201', 'Lec', '40', '2', '5'),
('CB 206', 'Lec', '40', '2', '6'),
('CB 307', 'Lec', '40', '2', '7'),
('CB 202', 'Lec', '40', '2', '8'),
('CB 207', 'Lec', '40', '2', '9'),
('CB 208', 'Lec', '40', '2', '10'),
('CB 304', 'Lec', '40', '2', '11'),
('CB 303', 'Lec', '40', '2', '12'),
('CB 404', 'Lec', '40', '2', '13'),
('CB 403', 'Lec', '40', '2', '14'),
('CB 104', 'Lec', '40', '2', '15'),
('CB 102', 'Lec', '40', '2', '16'),
('CB 401', 'Lec', '40', '2', '17'),
('CB 101', 'Lec', '40', '2', '18'),
('CB 402', 'Lec', '40', '2', '19'),
('CB 103', 'Lec', '40', '2', '20'),
('CB 302', 'Lec', '40', '2', '21'),
('CB 305', 'Lec', '40', '2', '22'),
('CB 301', 'Lec', '40', '2', '23'),
('CB 306', 'Lec', '40', '2', '24');

-- --------------------------------------------------------

--
-- Table structure for table `secretary`
--

CREATE TABLE `secretary` (
  `secretary_no` int(20) NOT NULL,
  `lname` varchar(20) NOT NULL,
  `fname` varchar(60) NOT NULL,
  `department` varchar(60) NOT NULL,
  `username` varchar(60) NOT NULL,
  `password` varchar(60) NOT NULL,
  `email` varchar(60) NOT NULL,
  `user_id` int(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sectionsubject_cbeam`
--

CREATE TABLE `sectionsubject_cbeam` (
  `section` varchar(60) NOT NULL,
  `subjectcode` varchar(60) NOT NULL,
  `type` varchar(60) NOT NULL,
  `subject` varchar(60) NOT NULL,
  `units` varchar(60) NOT NULL,
  `collage_id` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sectionsubject_cbeam`
--

INSERT INTO `sectionsubject_cbeam` (`section`, `subjectcode`, `type`, `subject`, `units`, `collage_id`) VALUES
('AT4C', 'Intaud', 'REG', 'Internal Auditing', '6', '2'),
('IS2A', 'Acctfin*', 'REG', 'Accounting and Financials', '3', '2'),
('H2B', 'Basiacc', 'REG', 'Basic Accounting', '3', '2'),
('AT4D', 'Intaud', 'REG', 'Internal Auditing', '6', '2'),
('AT2A', 'Macecon*', 'REG', 'Macroeconomic Theory & Practice', '3', '2'),
('F1A', 'Micecon', 'REG', 'Microeconomics', '3', '2'),
('A2A', 'Macecon', 'REG', 'Macroeconomics', '3', '2'),
('S3A', 'Ecointe', 'REG', 'International Economics', '3', '2'),
('M1B', 'Micecon', 'REG', 'Microeconomics', '3', '2'),
('S3A', 'Econman', 'REG', 'Managerial Economics', '3', '2'),
('AT4B', 'Mac-two', 'REG', 'Managerial Accounting II', '3', '2'),
('AT3A', 'Finman1', 'REG', 'Financial Management 1', '3', '2'),
('AT4A', 'Mac-two', 'REG', 'Managerial Accounting II', '3', '2'),
('AT4E', 'Accthes2', 'REG', 'Accounting Thesis, Part 2', '3', '2'),
('A4B', 'Manacon', 'REG', 'Management Consultancy', '3', '2'),
('AT1B', 'Basifin', 'REG', 'Basic Finance', '3', '2'),
('AT1A', 'Basifin', 'REG', 'Basic Finance', '3', '2'),
('C2A', 'EconTax', 'REG', 'Economics with Taxation and Land Reform', '3', '2'),
('A2B', 'Macecon', 'REG', 'Macroeconomics', '3', '2'),
('T2B', 'EconTax', 'REG', 'Economics with Taxation and Land Reform', '3', '2'),
('T2A', 'EconTax', 'REG', 'Economics with Taxation and Land Reform', '3', '2'),
('M2B', 'Fin-Acc', '', 'Financial Accounting', '3', '2'),
('M2C', 'Fin-Acc', '', 'Financial Accounting', '3', '2'),
('L2A', 'Statre1', 'REG', 'Statistical Research 1', '3', '2'),
('K2A', 'Micecon', 'REG', 'Microeconomics', '3', '2'),
('N2A', 'Micecon', 'REG', 'Microeconomics', '3', '2'),
('A1E', 'Funacco2', 'REG', 'Fundamentals of Accounting, Part 2', '6', '2'),
('A1D', 'Funacco2', 'REG', 'Fundamentals of Accounting, Part 2', '6', '2'),
('A4B', 'Acc-rev3', 'REG', 'Accounting Review, Part 3', '3', '2'),
('A4C', 'Acc-rev3', 'REG', 'Accounting Review, Part 3', '3', '2'),
('A4A', 'Acc-rev3', 'REG', 'Accounting Review, Part 3', '3', '2'),
('AT4A', 'Accthes2', 'REG', 'Accounting Thesis, Part 2', '3', '2'),
('AT4B', 'Accthes2', 'REG', 'Accounting Thesis, Part 2', '3', '2'),
('T2C', 'EconTax', '', 'Economics with Taxation and Land Reform', '3', '2'),
('A4D', 'Manacon', 'REG', 'Management Consultancy', '3', '2'),
('A4A', 'Manacon', 'REG', 'Management Consultancy', '3', '2'),
('A3B', 'Bus-tax*', 'REG', 'Business and Transfer Taxes', '3', '2'),
('A3A', 'Bus-tax*', 'REG', 'Business and Transfer Taxes', '3', '2'),
('AT4C', 'Accthes2', 'REG', 'Accounting Thesis, Part 2', '3', '2'),
('L3A', 'Negotin', 'REG', 'Negotiable Instruments', '3', '2'),
('F3A', 'Finres 1', 'REG', 'Financial Research 1', '3', '2'),
('F3C', 'Finres 1', 'REG', 'Financial Research 1', '3', '2'),
('M3D', 'Makres1', 'REG', 'Marketing Research 1', '3', '2'),
('M3C', 'Makres1', 'REG', 'Marketing Research 1', '3', '2'),
('M3E', 'Makres1', 'REG', 'Marketing Research 1', '3', '2'),
('A4A', 'C-audit**', 'REG', 'Auditing in CIS Environment', '3', '2'),
('A4B', 'C-audit**', 'REG', 'Auditing in CIS Environment', '3', '2'),
('A4C', 'C-audit**', 'REG', 'Auditing in CIS Environment', '3', '2'),
('AT2B', 'Fin-two**', 'REG', 'Financial Accounting & Reporting, Part 2', '6', '2'),
('AT2A', 'Fin-two**', 'REG', 'Financial Accounting & Reporting, Part 2', '6', '2'),
('IT2A', 'Actgpri', 'REG', 'Acounting Principles', '3', '2'),
('IS4A', 'Humani3', 'REG', 'Humanities 3', '3', '2'),
('AT3C', 'Compacc*', 'REG', 'Advance Computer Application for Accountants', '3', '2'),
('L4A', 'Lablaws', 'REG', 'Labor Laws & Social Legislation', '3', '2'),
('L4B', 'Lablaws', 'REG', 'Labor Laws & Social Legislation', '3', '2'),
('J2A', 'Basicon', 'REG', 'Basic Economics w/ Taxation & Agrarian Reform', '3', '2'),
('W2A', 'Basicon', 'REG', 'Basic Economics w/ Taxation & Agrarian Reform', '3', '2'),
('J2B', 'Basicon', 'REG', 'Basic Economics w/ Taxation & Agrarian Reform', '3', '2'),
('U3B', 'Basicon', 'REG', 'Basic Economics w/ Taxation & Agrarian Reform', '3', '2'),
('D2A', 'EconTax', 'REG', 'Economics with Taxation and Land Reform', '3', '2'),
('E4AS', 'Ecoplan', 'REG', 'Economics Planning and Strategy', '3', '2'),
('M1A', 'Micecon', 'REG', 'Microeconomics', '3', '2'),
('M1C', 'Micecon', 'REG', 'Microeconomics', '3', '2'),
('K2A', 'Busopp1', 'REG', 'Business Opportunities 1', '3', '2'),
('N2A', 'Busopp1', 'REG', 'Business Opportunities 1', '3', '2'),
('K3A', 'Buspla2', 'REG', 'Business Planning 2', '3', '2'),
('F2A', 'Enbudev', 'REG', 'Entrepreneurship and Business Development', '3', '2'),
('M2C', 'Enbudev', 'REG', 'Entrepreneurship and Business Development', '3', '2'),
('Y3A', 'Mgtele1', 'COM', 'Entrepreneurial Management', '3', '2'),
('S3A', 'Ecores1', 'REG', 'Economic Research 1', '3', '2'),
('F4B', 'Finprac', 'REG', 'Practicum / Work Integrated Learning', '6', '2'),
('M3F', 'Makres1', 'REG', 'Marketing Research 1', '3', '2'),
('M3A', 'Markstr', 'REG', 'Strategic Marketing Management', '3', '2'),
('M3B', 'Markstr', 'REG', 'Strategic Marketing Management', '3', '2'),
('M3C', 'Markstr', 'REG', 'Strategic Marketing Management', '3', '2'),
('M3D', 'Markstr', 'REG', 'Strategic Marketing Management', '3', '2'),
('E2AS', 'Mimecon', 'REG', 'Micro-Macro Economics', '3', '2'),
('U3A', 'Basicon', 'REG', 'Basic Economics w/ Taxation & Agrarian Reform', '3', '2'),
('O2A', 'EconTax', 'REG', 'Economics with Taxation and Land Reform', '3', '2'),
('V2A', 'EconTax', 'REG', 'Economics with Taxation and Land Reform', '3', '2'),
('L3A', 'Macecon', 'REG', 'Macroeconomics', '3', '2'),
('A3A', 'Adv-two*', 'REG', 'Advance Financial Accounting and Reporting, Part 2', '3', '2'),
('A3B', 'Adv-two*', 'REG', 'Advance Financial Accounting and Reporting, Part 2', '3', '2'),
('A4A', 'Buspoli*', 'REG', 'Business Policy & Strategy', '3', '2'),
('A4B', 'Buspoli*', 'REG', 'Business Policy & Strategy', '3', '2'),
('A4C', 'Buspoli*', 'REG', 'Business Policy & Strategy', '3', '2'),
('AT4B', 'Buspoli*', 'REG', 'Business Policy & Strategy', '3', '2'),
('AT4C', 'Buspoli*', 'REG', 'Business Policy & Strategy', '3', '2'),
('AT4D', 'Buspoli*', 'REG', 'Business Policy & Strategy', '3', '2'),
('A3A', 'Finman1', 'REG', 'Financial Management 1', '3', '2'),
('A3B', 'Finman1', 'REG', 'Financial Management 1', '3', '2'),
('AT4F', 'Accthes2', 'REG', 'Accounting Thesis, Part 2', '3', '2'),
('F3A', 'Fincapi', 'REG', 'Capital Markets', '3', '2'),
('F3B', 'Fincapi', 'REG', 'Capital Markets', '3', '2'),
('F3C', 'Fincapi', 'REG', 'Capital Markets', '3', '2'),
('F1A', 'Finmark', 'REG', 'Fin. Market w/ Prin. of Money,Credit & Banking', '3', '2'),
('M1A', 'Finmark', 'REG', 'Fin. Market w/ Prin. of Money,Credit & Banking', '3', '2'),
('M1B', 'Finmark', 'REG', 'Fin. Market w/ Prin. of Money,Credit & Banking', '3', '2'),
('S1A', 'Finmark', 'REG', 'Fin. Market w/ Prin. of Money,Credit & Banking', '3', '2'),
('S3A', 'Econpro', 'REG', 'Project Development Management', '3', '2'),
('S3A', 'Ecostra', 'REG', 'Strategic Corporate Development', '3', '2'),
('AT2C', 'Micecon*', 'REG', 'Microeconomic Theory & Practice', '3', '2'),
('A3A', 'Negotin', 'REG', 'Negotiable Instruments', '3', '2'),
('A3B', 'Negotin', 'REG', 'Negotiable Instruments', '3', '2'),
('L2A', 'Fin-Acc', '', 'Financial Accounting', '3', '2'),
('M2A', 'Fin-Acc', '', 'Financial Accounting', '3', '2'),
('D3A', 'IECostA', 'REG', 'IE Cost Accounting', '3', '2'),
('D3B', 'IECostA', 'REG', 'IE Cost Accounting', '3', '2'),
('IT2B', 'EconTax', 'REG', 'Economics with Taxation and Land Reform', '3', '2'),
('AT2B', 'Macecon', '', 'Macroeconomics', '3', '2'),
('AT4A', 'Salesag*', 'REG', 'Sales, Agency, Labor & Other Commercial Law', '3', '2'),
('AT4B', 'Salesag*', 'REG', 'Sales, Agency, Labor & Other Commercial Law', '3', '2'),
('K2A', 'Busifin', 'REG', 'Business Finance', '3', '2'),
('N2A', 'Busifin', 'REG', 'Business Finance', '3', '2'),
('M2B', 'Enbudev', 'REG', 'Entrepreneurship and Business Development', '3', '2'),
('K3A', 'Entele3', 'REG', 'Venture Capital', '3', '2'),
('N1A', 'Humborg', 'REG', 'Human Behavior in Organizations', '3', '2'),
('A1A', 'Humborg', 'REG', 'Human Behavior in Organizations', '3', '2'),
('A1C', 'Humborg', 'REG', 'Human Behavior in Organizations', '3', '2'),
('A1D', 'Humborg', 'REG', 'Human Behavior in Organizations', '3', '2'),
('AT1B', 'Humborg', 'REG', 'Human Behavior in Organizations', '3', '2'),
('M1A', 'Humborg', 'REG', 'Human Behavior in Organizations', '3', '2'),
('Y1A', 'Humborg', 'REG', 'Human Behavior in Organizations', '3', '2'),
('F4C', 'Finprac', 'REG', 'Practicum / Work Integrated Learning', '6', '2'),
('F3A', 'Finstra', 'REG', 'Strategic Financial Mangement', '3', '2'),
('F3B', 'Finstra', 'REG', 'Strategic Financial Mangement', '3', '2'),
('F3C', 'Finstra', 'REG', 'Strategic Financial Mangement', '3', '2'),
('A1C', 'Funacco2', 'REG', 'Fundamentals of Accounting, Part 2', '6', '2'),
('AT3B', 'Finman1', 'REG', 'Financial Management 1', '3', '2'),
('AT2C', 'Fin-two**', 'REG', 'Financial Accounting & Reporting, Part 2', '6', '2'),
('AT1A', 'Humborg', 'REG', 'Human Behavior in Organizations', '3', '2'),
('F1A', 'Humborg', 'REG', 'Human Behavior in Organizations', '3', '2'),
('J1A', 'Humborg', 'REG', 'Human Behavior in Organizations', '3', '2'),
('J1B', 'Humborg', 'REG', 'Human Behavior in Organizations', '3', '2'),
('L1A', 'Humborg', 'REG', 'Human Behavior in Organizations', '3', '2'),
('M1C', 'Humborg', 'REG', 'Human Behavior in Organizations', '3', '2'),
('A1B', 'Humborg', 'REG', 'Human Behavior in Organizations', '3', '2'),
('H1A', 'Humborg', 'REG', 'Human Behavior in Organizations', '3', '2'),
('M1B', 'Humborg', 'REG', 'Human Behavior in Organizations', '3', '2'),
('R1A', 'Humborg', 'REG', 'Human Behavior in Organizations', '3', '2'),
('Y4A', 'Mgtprac', 'REG', 'Practicum / Work Integrated Learning', '6', '2'),
('L4A', 'Envilaw', 'REG', 'Environmental Law', '3', '2'),
('L3A', 'Legwrit2', 'REG', 'Legal Writing 2', '3', '2'),
('AT4D', 'Salesag*', 'REG', 'Sales, Agency, Labor & Other Commercial Law', '3', '2'),
('A1A', 'Basifin', 'REG', 'Basic Finance', '3', '2'),
('A1B', 'Basifin', 'REG', 'Basic Finance', '3', '2'),
('F3B', 'Finres 1', 'REG', 'Financial Research 1', '3', '2'),
('F3D', 'Finres 1', 'REG', 'Financial Research 1', '3', '2'),
('Y2A', 'Micecon', 'REG', 'Microeconomics', '3', '2'),
('A2A', 'Oblicon', 'REG', 'Obligations and Contracts', '3', '2'),
('L2A', 'Statcon', 'REG', 'Statutory Construction', '3', '2'),
('AT3A', 'Bus-tax*', 'REG', 'Business and Transfer Taxes', '3', '2'),
('AT3B', 'Bus-tax*', 'REG', 'Business and Transfer Taxes', '3', '2'),
('K2A', 'CostAcc', 'REG', 'Cost Accounting', '3', '2'),
('AT3C', 'Bus-tax*', 'REG', 'Business and Transfer Taxes', '3', '2'),
('AT3D', 'Bus-tax*', 'REG', 'Business and Transfer Taxes', '3', '2'),
('N2A', 'CostAcc', 'REG', 'Cost Accounting', '3', '2'),
('L4A', 'Elecad', 'REG', 'Election Administration and Politics', '3', '2'),
('L4A', 'Elecad', 'REG', 'Election Administration and Politics', '3', '2'),
('L4B', 'Elecad', 'REG', 'Election Administration and Politics', '3', '2'),
('L4B', 'Elecad', 'REG', 'Election Administration and Politics', '3', '2'),
('L4B', 'Envilaw', 'REG', 'Environmental Law', '3', '2'),
('H2A', 'Basiacc', 'REG', 'Basic Accounting', '3', '2'),
('AT3D', 'Costing', 'REG', 'Accounting & Management of Cost', '6', '2'),
('L3A', 'Inc-Tax', 'REG', 'Philippine Tax System & Income Taxation', '3', '2'),
('AT4C', 'Mac-two', 'REG', 'Managerial Accounting II', '3', '2'),
('AT4D', 'Mac-two', 'REG', 'Managerial Accounting II', '3', '2'),
('AT3C', 'Lawbusi', 'REG', 'Law on Business Organizations', '3', '2'),
('AT3D', 'Lawbusi', 'REG', 'Law on Business Organizations', '3', '2'),
('Y3A', 'Mgtele2', 'REG', 'Logistic & Design', '3', '2'),
('AT4A', 'Intaud', 'REG', 'Internal Auditing', '6', '2'),
('AT4B', 'Intaud', 'REG', 'Internal Auditing', '6', '2'),
('A2A', 'Fin-two**', 'REG', 'Financial Accounting & Reporting, Part 2', '6', '2'),
('A2B', 'Fin-two**', 'REG', 'Financial Accounting & Reporting, Part 2', '6', '2'),
('A4A', 'Mac-two', 'REG', 'Managerial Accounting II', '3', '2'),
('A4B', 'Mac-two', 'REG', 'Managerial Accounting II', '3', '2'),
('A4C', 'Mac-two', 'REG', 'Managerial Accounting II', '3', '2'),
('L4A', 'Litlaw', 'REG', 'Introduction to Litigation', '3', '2'),
('L4B', 'Litlaw', 'REG', 'Introduction to Litigation', '3', '2'),
('M4E', 'Markpra', 'REG', 'Practicum / Work Integrated Learning', '6', '2'),
('M3A', 'Markret', 'REG', 'Retail Marketing Management', '3', '2'),
('M3B', 'Markret', 'REG', 'Retail Marketing Management', '3', '2'),
('M3C', 'Markret', 'REG', 'Retail Marketing Management', '3', '2'),
('M3D', 'Markret', 'REG', 'Retail Marketing Management', '3', '2'),
('M4A', 'Markpra', 'REG', 'Practicum / Work Integrated Learning', '6', '2'),
('M4B', 'Markpra', 'REG', 'Practicum / Work Integrated Learning', '6', '2'),
('M3A', 'Marksal', 'REG', 'Prof. Salesmanship and Sales Management', '3', '2'),
('M3B', 'Marksal', 'REG', 'Prof. Salesmanship and Sales Management', '3', '2');

-- --------------------------------------------------------

--
-- Table structure for table `sectionsubject_ceas`
--

CREATE TABLE `sectionsubject_ceas` (
  `section` varchar(60) NOT NULL,
  `subjectcode` varchar(60) NOT NULL,
  `type` varchar(60) NOT NULL,
  `subject` varchar(60) NOT NULL,
  `units` varchar(60) NOT NULL,
  `collage_id` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sectionsubject_cite`
--

CREATE TABLE `sectionsubject_cite` (
  `section` varchar(60) NOT NULL,
  `subjectcode` varchar(60) NOT NULL,
  `type` varchar(60) NOT NULL,
  `subject` varchar(60) NOT NULL,
  `units` varchar(60) NOT NULL,
  `collage_id` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sectionsubject_cithm`
--

CREATE TABLE `sectionsubject_cithm` (
  `section` varchar(60) NOT NULL,
  `subjectcode` varchar(60) NOT NULL,
  `type` varchar(60) NOT NULL,
  `subject` varchar(60) NOT NULL,
  `units` varchar(60) NOT NULL,
  `collage_id` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sectionsubject_con`
--

CREATE TABLE `sectionsubject_con` (
  `section` varchar(60) NOT NULL,
  `subjectcode` varchar(60) NOT NULL,
  `type` varchar(60) NOT NULL,
  `subject` varchar(60) NOT NULL,
  `units` varchar(60) NOT NULL,
  `collage_id` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `snumber` varchar(60) NOT NULL,
  `slname` varchar(60) NOT NULL,
  `sfname` varchar(60) NOT NULL,
  `smname` varchar(60) NOT NULL,
  `year` varchar(60) NOT NULL,
  `section` varchar(60) NOT NULL,
  `course` varchar(60) NOT NULL,
  `user_type` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`snumber`, `slname`, `sfname`, `smname`, `year`, `section`, `course`, `user_type`) VALUES
('2013324567', 'Danong', 'Arienna', 'Lozada', '4th', 'A4A', 'Bachelor of Science in Accountancy', 'student'),
('2013324567', 'Danong', 'Arienna', 'Lozada', '4th', 'A4A', 'Bachelor of Science in Accountancy', 'student'),
('2013324567', 'Danong', 'Arienna', 'Lozada', '4th', 'A4A', 'Bachelor of Science in Accountancy', 'student'),
('2013324567', 'Danong', 'Arienna', 'Lozada', '4th', 'A4A', 'Bachelor of Science in Accountancy', 'student'),
('2013324567', 'Danong', 'Arienna', 'Lozada', '4th', 'A4A', 'Bachelor of Science in Accountancy', 'student'),
('2013324567', 'Danong', 'Arienna', 'Lozada', '4th', 'A4A', 'Bachelor of Science in Accountancy', 'student'),
('2013324567', 'Danong', 'Arienna', 'Lozada', '4th', 'A4A', 'Bachelor of Science in Accountancy', 'student'),
('2013324567', 'Danong', 'Arienna', 'Lozada', '4th', 'A4A', 'Bachelor of Science in Accountancy', 'student'),
('2013332456', 'Maala', 'Gregor Sebastian', 'Dawin', '4th', 'A4B', 'Bachelor of Science in Accountancy', 'student'),
('2013332456', 'Maala', 'Gregor Sebastian', 'Dawin', '4th', 'A4B', 'Bachelor of Science in Accountancy', 'student'),
('2013332456', 'Maala', 'Gregor Sebastian', 'Dawin', '4th', 'A4B', 'Bachelor of Science in Accountancy', 'student'),
('2013332456', 'Maala', 'Gregor Sebastian', 'Dawin', '4th', 'A4B', 'Bachelor of Science in Accountancy', 'student'),
('2013332456', 'Maala', 'Gregor Sebastian', 'Dawin', '4th', 'A4B', 'Bachelor of Science in Accountancy', 'student'),
('2013332456', 'Maala', 'Gregor Sebastian', 'Dawin', '4th', 'A4B', 'Bachelor of Science in Accountancy', 'student'),
('2013332456', 'Maala', 'Gregor Sebastian', 'Dawin', '4th', 'A4B', 'Bachelor of Science in Accountancy', 'student'),
('2013332456', 'Maala', 'Gregor Sebastian', 'Dawin', '4th', 'A4B', 'Bachelor of Science in Accountancy', 'student'),
('2014324566', 'Siangco', 'Mary Grace', 'Punzalan', '3rd', 'A3A', 'Bachelor of Science in Accountancy', 'student'),
('2014324566', 'Siangco', 'Mary Grace', 'Punzalan', '3rd', 'A3A', 'Bachelor of Science in Accountancy', 'student'),
('2014324566', 'Siangco', 'Mary Grace', 'Punzalan', '3rd', 'A3A', 'Bachelor of Science in Accountancy', 'student'),
('2014324566', 'Siangco', 'Mary Grace', 'Punzalan', '3rd', 'A3A', 'Bachelor of Science in Accountancy', 'student'),
('2014324566', 'Siangco', 'Mary Grace', 'Punzalan', '3rd', 'A3A', 'Bachelor of Science in Accountancy', 'student'),
('2014324566', 'Siangco', 'Mary Grace', 'Punzalan', '3rd', 'A3A', 'Bachelor of Science in Accountancy', 'student'),
('2014324566', 'Siangco', 'Mary Grace', 'Punzalan', '3rd', 'A3A', 'Bachelor of Science in Accountancy', 'student'),
('2014324567', 'Espeleta', 'Lovely May', 'Go', '3rd', 'A3A', 'Bachelor of Science in Accountancy', 'student'),
('2014324567', 'Espeleta', 'Lovely May', 'Go', '3rd', 'A3A', 'Bachelor of Science in Accountancy', 'student'),
('2014324567', 'Espeleta', 'Lovely May', 'Go', '3rd', 'A3A', 'Bachelor of Science in Accountancy', 'student'),
('2014324567', 'Espeleta', 'Lovely May', 'Go', '3rd', 'A3A', 'Bachelor of Science in Accountancy', 'student'),
('2014324567', 'Espeleta', 'Lovely May', 'Go', '3rd', 'A3A', 'Bachelor of Science in Accountancy', 'student'),
('2014324567', 'Espeleta', 'Lovely May', 'Go', '3rd', 'A3A', 'Bachelor of Science in Accountancy', 'student'),
('2014324567', 'Espeleta', 'Lovely May', 'Go', '3rd', 'A3A', 'Bachelor of Science in Accountancy', 'student'),
('2015324567', 'Agnes', 'Christian', 'Rosales', '2nd', 'A2A', 'Bachelor of Science in Accountancy', 'student'),
('2015324567', 'Agnes', 'Christian', 'Rosales', '2nd', 'A2A', 'Bachelor of Science in Accountancy', 'student'),
('2015324567', 'Agnes', 'Christian', 'Rosales', '2nd', 'A2A', 'Bachelor of Science in Accountancy', 'student'),
('2015324567', 'Agnes', 'Christian', 'Rosales', '2nd', 'A2A', 'Bachelor of Science in Accountancy', 'student'),
('2015324567', 'Agnes', 'Christian', 'Rosales', '2nd', 'A2A', 'Bachelor of Science in Accountancy', 'student'),
('2015324567', 'Agnes', 'Christian', 'Rosales', '2nd', 'A2A', 'Bachelor of Science in Accountancy', 'student'),
('2015324567', 'Agnes', 'Christian', 'Rosales', '2nd', 'A2A', 'Bachelor of Science in Accountancy', 'student'),
('2015324567', 'Agnes', 'Christian', 'Rosales', '2nd', 'A2A', 'Bachelor of Science in Accountancy', 'student'),
('2015324567', 'Agnes', 'Christian', 'Rosales', '2nd', 'A2A', 'Bachelor of Science in Accountancy', 'student'),
('2015324568', 'Cay', 'Annadel', 'Bustos', '2nd', 'A2B', 'Bachelor of Science in Accountancy', 'student'),
('2015324568', 'Cay', 'Annadel', 'Bustos', '2nd', 'A2B', 'Bachelor of Science in Accountancy', 'student'),
('2015324568', 'Cay', 'Annadel', 'Bustos', '2nd', 'A2B', 'Bachelor of Science in Accountancy', 'student'),
('2015324568', 'Cay', 'Annadel', 'Bustos', '2nd', 'A2B', 'Bachelor of Science in Accountancy', 'student'),
('2015324568', 'Cay', 'Annadel', 'Bustos', '2nd', 'A2B', 'Bachelor of Science in Accountancy', 'student'),
('2015324568', 'Cay', 'Annadel', 'Bustos', '2nd', 'A2B', 'Bachelor of Science in Accountancy', 'student'),
('2015324568', 'Cay', 'Annadel', 'Bustos', '2nd', 'A2B', 'Bachelor of Science in Accountancy', 'student'),
('2015324568', 'Cay', 'Annadel', 'Bustos', '2nd', 'A2B', 'Bachelor of Science in Accountancy', 'student'),
('2015324568', 'Cay', 'Annadel', 'Bustos', '2nd', 'A2B', 'Bachelor of Science in Accountancy', 'student'),
('2015324569', 'Bondad', 'Banjo', 'Belandres', '2nd', 'A2C', 'Bachelor of Science in Accountancy', 'student'),
('2015324569', 'Bondad', 'Banjo', 'Belandres', '2nd', 'A2C', 'Bachelor of Science in Accountancy', 'student'),
('2015324569', 'Bondad', 'Banjo', 'Belandres', '2nd', 'A2C', 'Bachelor of Science in Accountancy', 'student'),
('2015324569', 'Bondad', 'Banjo', 'Belandres', '2nd', 'A2C', 'Bachelor of Science in Accountancy', 'student'),
('2015324569', 'Bondad', 'Banjo', 'Belandres', '2nd', 'A2C', 'Bachelor of Science in Accountancy', 'student'),
('2015324569', 'Bondad', 'Banjo', 'Belandres', '2nd', 'A2C', 'Bachelor of Science in Accountancy', 'student'),
('2015324569', 'Bondad', 'Banjo', 'Belandres', '2nd', 'A2C', 'Bachelor of Science in Accountancy', 'student'),
('2015324569', 'Bondad', 'Banjo', 'Belandres', '2nd', 'A2C', 'Bachelor of Science in Accountancy', 'student'),
('2015324569', 'Bondad', 'Banjo', 'Belandres', '2nd', 'A2C', 'Bachelor of Science in Accountancy', 'student'),
('2015345678', 'Marquez', 'Carl Angelo ', 'Sikat', '1st', 'A1A', 'Bachelor of Science in Accountancy', 'student'),
('2015345678', 'Marquez', 'Carl Angelo ', 'Sikat', '1st', 'A1A', 'Bachelor of Science in Accountancy', 'student'),
('2015345678', 'Marquez', 'Carl Angelo ', 'Sikat', '1st', 'A1A', 'Bachelor of Science in Accountancy', 'student'),
('2015345678', 'Marquez', 'Carl Angelo ', 'Sikat', '1st', 'A1A', 'Bachelor of Science in Accountancy', 'student'),
('2015345678', 'Marquez', 'Carl Angelo ', 'Sikat', '1st', 'A1A', 'Bachelor of Science in Accountancy', 'student'),
('2015345678', 'Marquez', 'Carl Angelo ', 'Sikat', '1st', 'A1A', 'Bachelor of Science in Accountancy', 'student'),
('2015345678', 'Marquez', 'Carl Angelo ', 'Sikat', '1st', 'A1A', 'Bachelor of Science in Accountancy', 'student'),
('2015345678', 'Marquez', 'Carl Angelo ', 'Sikat', '1st', 'A1A', 'Bachelor of Science in Accountancy', 'student'),
('2015345678', 'Marquez', 'Carl Angelo ', 'Sikat', '1st', 'A1A', 'Bachelor of Science in Accountancy', 'student'),
('2015345278', 'Blanco', 'Ismael ', 'Borja', '1st', 'A1B', 'Bachelor of Science in Accountancy', 'student'),
('2015345278', 'Blanco', 'Ismael ', 'Borja', '1st', 'A1B', 'Bachelor of Science in Accountancy', 'student'),
('2015345278', 'Blanco', 'Ismael ', 'Borja', '1st', 'A1B', 'Bachelor of Science in Accountancy', 'student'),
('2015345278', 'Blanco', 'Ismael ', 'Borja', '1st', 'A1B', 'Bachelor of Science in Accountancy', 'student'),
('2015345278', 'Blanco', 'Ismael ', 'Borja', '1st', 'A1B', 'Bachelor of Science in Accountancy', 'student'),
('2015345278', 'Blanco', 'Ismael ', 'Borja', '1st', 'A1B', 'Bachelor of Science in Accountancy', 'student'),
('2015345278', 'Blanco', 'Ismael ', 'Borja', '1st', 'A1B', 'Bachelor of Science in Accountancy', 'student'),
('2015345278', 'Blanco', 'Ismael ', 'Borja', '1st', 'A1B', 'Bachelor of Science in Accountancy', 'student'),
('2015345278', 'Blanco', 'Ismael ', 'Borja', '1st', 'A1B', 'Bachelor of Science in Accountancy', 'student'),
('2015000123', 'Fabian', 'Justin Michael', 'Sanciangco', '1st', 'A1C', 'Bachelor of Science in Accountancy', 'student'),
('2015000123', 'Fabian', 'Justin Michael', 'Sanciangco', '1st', 'A1C', 'Bachelor of Science in Accountancy', 'student'),
('2015000123', 'Fabian', 'Justin Michael', 'Sanciangco', '1st', 'A1C', 'Bachelor of Science in Accountancy', 'student'),
('2015000123', 'Fabian', 'Justin Michael', 'Sanciangco', '1st', 'A1C', 'Bachelor of Science in Accountancy', 'student'),
('2015000123', 'Fabian', 'Justin Michael', 'Sanciangco', '1st', 'A1C', 'Bachelor of Science in Accountancy', 'student'),
('2015000123', 'Fabian', 'Justin Michael', 'Sanciangco', '1st', 'A1C', 'Bachelor of Science in Accountancy', 'student'),
('2015000123', 'Fabian', 'Justin Michael', 'Sanciangco', '1st', 'A1C', 'Bachelor of Science in Accountancy', 'student'),
('2015000123', 'Fabian', 'Justin Michael', 'Sanciangco', '1st', 'A1C', 'Bachelor of Science in Accountancy', 'student'),
('2015000123', 'Fabian', 'Justin Michael', 'Sanciangco', '1st', 'A1C', 'Bachelor of Science in Accountancy', 'student'),
('2016567890', 'Andres', 'Robet', 'Fandilio', '1st', 'Y1A', 'Bachelor of Science in Management Technology', 'student'),
('2016567890', 'Andres', 'Robet', 'Fandilio', '1st', 'Y1A', 'Bachelor of Science in Management Technology', 'student'),
('2016567890', 'Andres', 'Robet', 'Fandilio', '1st', 'Y1A', 'Bachelor of Science in Management Technology', 'student'),
('2016567890', 'Andres', 'Robet', 'Fandilio', '1st', 'Y1A', 'Bachelor of Science in Management Technology', 'student'),
('2016567890', 'Andres', 'Robet', 'Fandilio', '1st', 'Y1A', 'Bachelor of Science in Management Technology', 'student'),
('2016567890', 'Andres', 'Robet', 'Fandilio', '1st', 'Y1A', 'Bachelor of Science in Management Technology', 'student'),
('2016567890', 'Andres', 'Robet', 'Fandilio', '1st', 'Y1A', 'Bachelor of Science in Management Technology', 'student'),
('2016567890', 'Andres', 'Robet', 'Fandilio', '1st', 'Y1A', 'Bachelor of Science in Management Technology', 'student'),
('2016567890', 'Andres', 'Robet', 'Fandilio', '1st', 'Y1A', 'Bachelor of Science in Management Technology', 'student'),
('2015567890', 'Magtibay', 'Kevin ', 'Quinto', '2nd', 'Y2A', 'Bachelor of Science in Management Technology', 'student'),
('2015567890', 'Magtibay', 'Kevin ', 'Quinto', '2nd', 'Y2A', 'Bachelor of Science in Management Technology', 'student'),
('2015567890', 'Magtibay', 'Kevin ', 'Quinto', '2nd', 'Y2A', 'Bachelor of Science in Management Technology', 'student'),
('2015567890', 'Magtibay', 'Kevin ', 'Quinto', '2nd', 'Y2A', 'Bachelor of Science in Management Technology', 'student'),
('2015567890', 'Magtibay', 'Kevin ', 'Quinto', '2nd', 'Y2A', 'Bachelor of Science in Management Technology', 'student'),
('2015567890', 'Magtibay', 'Kevin ', 'Quinto', '2nd', 'Y2A', 'Bachelor of Science in Management Technology', 'student'),
('2015567890', 'Magtibay', 'Kevin ', 'Quinto', '2nd', 'Y2A', 'Bachelor of Science in Management Technology', 'student'),
('2015567890', 'Magtibay', 'Kevin ', 'Quinto', '2nd', 'Y2A', 'Bachelor of Science in Management Technology', 'student'),
('2014567890', 'Porras', 'Zarrah', 'Ramirez', '3rd', 'Y3A', 'Bachelor of Science in Management Technology', 'student'),
('2014567890', 'Porras', 'Zarrah', 'Ramirez', '3rd', 'Y3A', 'Bachelor of Science in Management Technology', 'student'),
('2014567890', 'Porras', 'Zarrah', 'Ramirez', '3rd', 'Y3A', 'Bachelor of Science in Management Technology', 'student'),
('2014567890', 'Porras', 'Zarrah', 'Ramirez', '3rd', 'Y3A', 'Bachelor of Science in Management Technology', 'student'),
('2014567890', 'Porras', 'Zarrah', 'Ramirez', '3rd', 'Y3A', 'Bachelor of Science in Management Technology', 'student'),
('2014567890', 'Porras', 'Zarrah', 'Ramirez', '3rd', 'Y3A', 'Bachelor of Science in Management Technology', 'student'),
('2014567890', 'Porras', 'Zarrah', 'Ramirez', '3rd', 'Y3A', 'Bachelor of Science in Management Technology', 'student'),
('2014567890', 'Porras', 'Zarrah', 'Ramirez', '3rd', 'Y3A', 'Bachelor of Science in Management Technology', 'student'),
('2013567890', 'Magundayao', 'Lily Beth', 'Carro', '4th', 'Y4A', 'Bachelor of Science in Management Technology', 'student'),
('2013567890', 'Magundayao', 'Lily Beth', 'Carro', '4th', 'Y4A', 'Bachelor of Science in Management Technology', 'student'),
('2016111234', 'Pineda', 'Arlene Marie', 'Sy', '1st', 'AT1A', 'Bachelor of Science in Accounting Techology', 'student'),
('2016111234', 'Pineda', 'Arlene Marie', 'Sy', '1st', 'AT1A', 'Bachelor of Science in Accounting Techology', 'student'),
('2016111234', 'Pineda', 'Arlene Marie', 'Sy', '1st', 'AT1A', 'Bachelor of Science in Accounting Techology', 'student'),
('2016111234', 'Pineda', 'Arlene Marie', 'Sy', '1st', 'AT1A', 'Bachelor of Science in Accounting Techology', 'student'),
('2016111234', 'Pineda', 'Arlene Marie', 'Sy', '1st', 'AT1A', 'Bachelor of Science in Accounting Techology', 'student'),
('2016111234', 'Pineda', 'Arlene Marie', 'Sy', '1st', 'AT1A', 'Bachelor of Science in Accounting Techology', 'student'),
('2016111234', 'Pineda', 'Arlene Marie', 'Sy', '1st', 'AT1A', 'Bachelor of Science in Accounting Techology', 'student'),
('2016111234', 'Pineda', 'Arlene Marie', 'Sy', '1st', 'AT1A', 'Bachelor of Science in Accounting Techology', 'student'),
('2016111234', 'Pineda', 'Arlene Marie', 'Sy', '1st', 'AT1A', 'Bachelor of Science in Accounting Techology', 'student'),
('2016112234', 'Cruz', 'Angelo ', 'Baylon', '1st', 'AT1B', 'Bachelor of Science in Accounting Techology', 'student'),
('2016112234', 'Cruz', 'Angelo ', 'Baylon', '1st', 'AT1B', 'Bachelor of Science in Accounting Techology', 'student'),
('2016112234', 'Cruz', 'Angelo ', 'Baylon', '1st', 'AT1B', 'Bachelor of Science in Accounting Techology', 'student'),
('2016112234', 'Cruz', 'Angelo ', 'Baylon', '1st', 'AT1B', 'Bachelor of Science in Accounting Techology', 'student'),
('2016112234', 'Cruz', 'Angelo ', 'Baylon', '1st', 'AT1B', 'Bachelor of Science in Accounting Techology', 'student'),
('2016112234', 'Cruz', 'Angelo ', 'Baylon', '1st', 'AT1B', 'Bachelor of Science in Accounting Techology', 'student'),
('2016112234', 'Cruz', 'Angelo ', 'Baylon', '1st', 'AT1B', 'Bachelor of Science in Accounting Techology', 'student'),
('2016112234', 'Cruz', 'Angelo ', 'Baylon', '1st', 'AT1B', 'Bachelor of Science in Accounting Techology', 'student'),
('2016112234', 'Cruz', 'Angelo ', 'Baylon', '1st', 'AT1B', 'Bachelor of Science in Accounting Techology', 'student'),
('2016113234', 'Sanzhez', 'Girly', 'Iranzo', '1st', 'AT1C', 'Bachelor of Science in Accounting Techology', 'student'),
('2016113234', 'Sanzhez', 'Girly', 'Iranzo', '1st', 'AT1C', 'Bachelor of Science in Accounting Techology', 'student'),
('2016113234', 'Sanzhez', 'Girly', 'Iranzo', '1st', 'AT1C', 'Bachelor of Science in Accounting Techology', 'student'),
('2016113234', 'Sanzhez', 'Girly', 'Iranzo', '1st', 'AT1C', 'Bachelor of Science in Accounting Techology', 'student'),
('2016113234', 'Sanzhez', 'Girly', 'Iranzo', '1st', 'AT1C', 'Bachelor of Science in Accounting Techology', 'student'),
('2016113234', 'Sanzhez', 'Girly', 'Iranzo', '1st', 'AT1C', 'Bachelor of Science in Accounting Techology', 'student'),
('2016113234', 'Sanzhez', 'Girly', 'Iranzo', '1st', 'AT1C', 'Bachelor of Science in Accounting Techology', 'student'),
('2016113234', 'Sanzhez', 'Girly', 'Iranzo', '1st', 'AT1C', 'Bachelor of Science in Accounting Techology', 'student'),
('2016113234', 'Sanzhez', 'Girly', 'Iranzo', '1st', 'AT1C', 'Bachelor of Science in Accounting Techology', 'student'),
('2015111234', 'Guia', 'Mikee', 'Quizon', '2nd', 'AT2A', 'Bachelor of Science in Accounting Techology', 'student'),
('2015111234', 'Guia', 'Mikee', 'Quizon', '2nd', 'AT2A', 'Bachelor of Science in Accounting Techology', 'student'),
('2015111234', 'Guia', 'Mikee', 'Quizon', '2nd', 'AT2A', 'Bachelor of Science in Accounting Techology', 'student'),
('2015111234', 'Guia', 'Mikee', 'Quizon', '2nd', 'AT2A', 'Bachelor of Science in Accounting Techology', 'student'),
('2015111234', 'Guia', 'Mikee', 'Quizon', '2nd', 'AT2A', 'Bachelor of Science in Accounting Techology', 'student'),
('2015111234', 'Guia', 'Mikee', 'Quizon', '2nd', 'AT2A', 'Bachelor of Science in Accounting Techology', 'student'),
('2015111234', 'Guia', 'Mikee', 'Quizon', '2nd', 'AT2A', 'Bachelor of Science in Accounting Techology', 'student'),
('2015111234', 'Guia', 'Mikee', 'Quizon', '2nd', 'AT2A', 'Bachelor of Science in Accounting Techology', 'student'),
('2015111234', 'Guia', 'Mikee', 'Quizon', '2nd', 'AT2A', 'Bachelor of Science in Accounting Techology', 'student'),
('2015112234', 'Velasquez', 'Hanna Grace ', 'Oba', '2nd', 'AT2B', 'Bachelor of Science in Accounting Techology', 'student'),
('2015112234', 'Velasquez', 'Hanna Grace ', 'Oba', '2nd', 'AT2B', 'Bachelor of Science in Accounting Techology', 'student'),
('2015112234', 'Velasquez', 'Hanna Grace ', 'Oba', '2nd', 'AT2B', 'Bachelor of Science in Accounting Techology', 'student'),
('2015112234', 'Velasquez', 'Hanna Grace ', 'Oba', '2nd', 'AT2B', 'Bachelor of Science in Accounting Techology', 'student'),
('2015112234', 'Velasquez', 'Hanna Grace ', 'Oba', '2nd', 'AT2B', 'Bachelor of Science in Accounting Techology', 'student'),
('2015112234', 'Velasquez', 'Hanna Grace ', 'Oba', '2nd', 'AT2B', 'Bachelor of Science in Accounting Techology', 'student'),
('2015112234', 'Velasquez', 'Hanna Grace ', 'Oba', '2nd', 'AT2B', 'Bachelor of Science in Accounting Techology', 'student'),
('2015112234', 'Velasquez', 'Hanna Grace ', 'Oba', '2nd', 'AT2B', 'Bachelor of Science in Accounting Techology', 'student'),
('2015112234', 'Velasquez', 'Hanna Grace ', 'Oba', '2nd', 'AT2B', 'Bachelor of Science in Accounting Techology', 'student'),
('2015113234', 'Dizon', 'Maricar', 'Clemente', '2nd', 'AT2C', 'Bachelor of Science in Accounting Techology', 'student'),
('2015113234', 'Dizon', 'Maricar', 'Clemente', '2nd', 'AT2C', 'Bachelor of Science in Accounting Techology', 'student'),
('2015113234', 'Dizon', 'Maricar', 'Clemente', '2nd', 'AT2C', 'Bachelor of Science in Accounting Techology', 'student'),
('2015113234', 'Dizon', 'Maricar', 'Clemente', '2nd', 'AT2C', 'Bachelor of Science in Accounting Techology', 'student'),
('2015113234', 'Dizon', 'Maricar', 'Clemente', '2nd', 'AT2C', 'Bachelor of Science in Accounting Techology', 'student'),
('2015113234', 'Dizon', 'Maricar', 'Clemente', '2nd', 'AT2C', 'Bachelor of Science in Accounting Techology', 'student'),
('2015113234', 'Dizon', 'Maricar', 'Clemente', '2nd', 'AT2C', 'Bachelor of Science in Accounting Techology', 'student'),
('2015113234', 'Dizon', 'Maricar', 'Clemente', '2nd', 'AT2C', 'Bachelor of Science in Accounting Techology', 'student'),
('2015113234', 'Dizon', 'Maricar', 'Clemente', '2nd', 'AT2C', 'Bachelor of Science in Accounting Techology', 'student'),
('2014111234', 'Zamora', 'Danielle Anne', 'Guazon', '3rd', 'AT3A', 'Bachelor of Science in Accounting Techology', 'student'),
('2014111234', 'Zamora', 'Danielle Anne', 'Guazon', '3rd', 'AT3A', 'Bachelor of Science in Accounting Techology', 'student'),
('2014111234', 'Zamora', 'Danielle Anne', 'Guazon', '3rd', 'AT3A', 'Bachelor of Science in Accounting Techology', 'student'),
('2014111234', 'Zamora', 'Danielle Anne', 'Guazon', '3rd', 'AT3A', 'Bachelor of Science in Accounting Techology', 'student'),
('2014111234', 'Zamora', 'Danielle Anne', 'Guazon', '3rd', 'AT3A', 'Bachelor of Science in Accounting Techology', 'student'),
('2014111234', 'Zamora', 'Danielle Anne', 'Guazon', '3rd', 'AT3A', 'Bachelor of Science in Accounting Techology', 'student'),
('2014111234', 'Zamora', 'Danielle Anne', 'Guazon', '3rd', 'AT3A', 'Bachelor of Science in Accounting Techology', 'student'),
('2014111234', 'Zamora', 'Danielle Anne', 'Guazon', '3rd', 'AT3A', 'Bachelor of Science in Accounting Techology', 'student'),
('2014112234', 'Hernandez', 'Nicole Jean', 'Pamplona', '3rd', 'AT3B', 'Bachelor of Science in Accounting Techology', 'student'),
('2014112234', 'Hernandez', 'Nicole Jean', 'Pamplona', '3rd', 'AT3B', 'Bachelor of Science in Accounting Techology', 'student'),
('2014112234', 'Hernandez', 'Nicole Jean', 'Pamplona', '3rd', 'AT3B', 'Bachelor of Science in Accounting Techology', 'student'),
('2014112234', 'Hernandez', 'Nicole Jean', 'Pamplona', '3rd', 'AT3B', 'Bachelor of Science in Accounting Techology', 'student'),
('2014112234', 'Hernandez', 'Nicole Jean', 'Pamplona', '3rd', 'AT3B', 'Bachelor of Science in Accounting Techology', 'student'),
('2014112234', 'Hernandez', 'Nicole Jean', 'Pamplona', '3rd', 'AT3B', 'Bachelor of Science in Accounting Techology', 'student'),
('2014112234', 'Hernandez', 'Nicole Jean', 'Pamplona', '3rd', 'AT3B', 'Bachelor of Science in Accounting Techology', 'student'),
('2014112234', 'Hernandez', 'Nicole Jean', 'Pamplona', '3rd', 'AT3B', 'Bachelor of Science in Accounting Techology', 'student'),
('2014113234', 'Calpe', 'John Andrew', 'Sumabe', '3rd', 'AT3C', 'Bachelor of Science in Accounting Techology', 'student'),
('2014113234', 'Calpe', 'John Andrew', 'Sumabe', '3rd', 'AT3C', 'Bachelor of Science in Accounting Techology', 'student'),
('2014113234', 'Calpe', 'John Andrew', 'Sumabe', '3rd', 'AT3C', 'Bachelor of Science in Accounting Techology', 'student'),
('2014113234', 'Calpe', 'John Andrew', 'Sumabe', '3rd', 'AT3C', 'Bachelor of Science in Accounting Techology', 'student'),
('2014113234', 'Calpe', 'John Andrew', 'Sumabe', '3rd', 'AT3C', 'Bachelor of Science in Accounting Techology', 'student'),
('2014113234', 'Calpe', 'John Andrew', 'Sumabe', '3rd', 'AT3C', 'Bachelor of Science in Accounting Techology', 'student'),
('2014113234', 'Calpe', 'John Andrew', 'Sumabe', '3rd', 'AT3C', 'Bachelor of Science in Accounting Techology', 'student'),
('2014113234', 'Calpe', 'John Andrew', 'Sumabe', '3rd', 'AT3C', 'Bachelor of Science in Accounting Techology', 'student'),
('2013111234', 'De Guzman ', 'Sheila May', 'Luna', '4th', 'AT4A', 'Bachelor of Science in Accounting Techology', 'student'),
('2013111234', 'De Guzman ', 'Sheila May', 'Luna', '4th', 'AT4A', 'Bachelor of Science in Accounting Techology', 'student'),
('2013111234', 'De Guzman ', 'Sheila May', 'Luna', '4th', 'AT4A', 'Bachelor of Science in Accounting Techology', 'student'),
('2013111234', 'De Guzman ', 'Sheila May', 'Luna', '4th', 'AT4A', 'Bachelor of Science in Accounting Techology', 'student'),
('2013111234', 'De Guzman ', 'Sheila May', 'Luna', '4th', 'AT4A', 'Bachelor of Science in Accounting Techology', 'student'),
('2013111234', 'De Guzman ', 'Sheila May', 'Luna', '4th', 'AT4A', 'Bachelor of Science in Accounting Techology', 'student'),
('2013112234', 'Kalumpit', 'Katherine', 'Bagon', '4th', 'AT4B', 'Bachelor of Science in Accounting Techology', 'student'),
('2013112234', 'Kalumpit', 'Katherine', 'Bagon', '4th', 'AT4B', 'Bachelor of Science in Accounting Techology', 'student'),
('2013112234', 'Kalumpit', 'Katherine', 'Bagon', '4th', 'AT4B', 'Bachelor of Science in Accounting Techology', 'student'),
('2013112234', 'Kalumpit', 'Katherine', 'Bagon', '4th', 'AT4B', 'Bachelor of Science in Accounting Techology', 'student'),
('2013112234', 'Kalumpit', 'Katherine', 'Bagon', '4th', 'AT4B', 'Bachelor of Science in Accounting Techology', 'student'),
('2013112234', 'Kalumpit', 'Katherine', 'Bagon', '4th', 'AT4B', 'Bachelor of Science in Accounting Techology', 'student'),
('2013113234', 'Barbosa', 'Carlos', 'Gamboa', '4th', 'AT4C', 'Bachelor of Science in Accounting Techology', 'student');

-- --------------------------------------------------------

--
-- Table structure for table `time_finals`
--

CREATE TABLE `time_finals` (
  `start` varchar(60) NOT NULL,
  `end` varchar(60) NOT NULL,
  `semester` varchar(60) NOT NULL,
  `semester_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `time_finals`
--

INSERT INTO `time_finals` (`start`, `end`, `semester`, `semester_type`) VALUES
('8:00 AM', '9:30 AM', '2', '1'),
('9:35 AM', '11:05 AM', '2', '1'),
('11:10 AM', '12:40 PM', '2', '1'),
('12:45 PM', '2:15 PM', '2', '1'),
('2:20 PM', '3:50 PM', '2', '1'),
('3:55 PM', '5:25 PM', '2', '1'),
('5:30 PM', '7:00 PM', '2', '1'),
('8:00 AM', '10:30 AM', '2', '2'),
('10:45 AM', '1:15 PM', '2', '2'),
('1:30 PM', '4:00 PM', '2', '2'),
('4:15 PM', '6:45 PM', '2', '2');

-- --------------------------------------------------------

--
-- Table structure for table `time_midterm`
--

CREATE TABLE `time_midterm` (
  `start` varchar(60) NOT NULL,
  `end` varchar(60) NOT NULL,
  `semester` varchar(60) NOT NULL,
  `semester_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `time_midterm`
--

INSERT INTO `time_midterm` (`start`, `end`, `semester`, `semester_type`) VALUES
('8:00 AM', '9:00 AM', '1', '1'),
('9:05 AM', '10:05 AM', '1', '1'),
('10:10 AM', '11:10 AM', '1', '1'),
('11:15 AM', '12:15 PM', '1', '1'),
('12:20 PM', '1:20 PM', '1', '1'),
('1:25 PM', '2:25 PM', '1', '1'),
('2:30 PM', '3:30 PM', '1', '1'),
('3:35 PM', '4:35 PM', '1', '1'),
('4:40 PM', '5:40 PM', '1', '1'),
('5:45 PM', '6:45 PM', '1', '1'),
('8:00 AM', '10:00 AM', '1', '2'),
('10:15 AM', '12:15 PM', '1', '2'),
('12:30 PM', '2:30 PM', '1', '2'),
('2:45 PM', '4:45 PM', '1', '2'),
('5:00 PM', '7:00 PM', '1', '2');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` varchar(20) NOT NULL,
  `user_pass` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `user_email` varchar(50) NOT NULL,
  `college_name` enum('Cite','Cbeam','Ceas','Cithm','Con') NOT NULL,
  `user_type` enum('Secretary','Dean','Faculty','Student') NOT NULL,
  `contact_number` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_pass`, `first_name`, `middle_name`, `last_name`, `user_email`, `college_name`, `user_type`, `contact_number`) VALUES
('1020304050', 'dlsl123', 'cite', 'cite', 'cite', 'cite@yahoo.com', 'Cite', 'Secretary', '09999999999'),
('1020304051', 'dlsl123', 'cbeam', 'cbeam', 'cbeam', 'cbeam@yahoo.com', 'Cbeam', 'Secretary', '09999999999'),
('1020304052', 'dlsl123', 'ceas', 'ceas', 'ceas', 'ceas@yahoo.com', 'Ceas', 'Secretary', '09999999999'),
('1020304053', 'dlsl123', 'cithm', 'cithm', 'cithm', 'cithm@yahoo.com', 'Cithm', 'Secretary', '09999999999'),
('1020304054', 'dlsl123', 'con', 'con', 'con', 'con@yahoo.com', 'Con', 'Secretary', '09999999999'),
('1020304055', 'dlsl123', 'faculty', 'faculty', 'faculty', 'faculty@yahoo.com', 'Cite', 'Faculty', '09999999999'),
('2013126351', 'dlsl123', 'Carlo Andrew', 'Villanueva', 'Calpe', 'carloandrew.calpe@yahoo.com', 'Cite', 'Student', '09152293144');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
