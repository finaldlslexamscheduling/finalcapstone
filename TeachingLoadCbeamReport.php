 <?php 

include "fpdf/fpdf.php";
include "pdo.php";

//echo "<pre>";
//print_r($_POST);

// $range = explode(" - ", $_POST['daterange']);
// //print_r($range);

// $range[0] = explode(" ",$range[0])[0];
// $range[1] = explode(" ",$range[1])[0];
//echo $range[0];

// if($_POST["location"]=="0")
// {
	$stmt = $dbh->prepare("SELECT * FROM teaching_load_cbeam order by section and faculty_name ");
	// $stmt->bindParam(':minDate', $range[0]);
	// $stmt->bindParam(':maxDate', $range[1]);
	// $name = "All stores";
// }
// else
// {
// 	$stmt = $dbh->prepare("SELECT * FROM purchase_log a LEFT JOIN studnum_rfid_info b ON a.superId = b.superId WHERE location = :location AND updatedAt BETWEEN str_to_date(:minDate,'%m/%d/%Y') AND str_to_date(:maxDate,'%m/%d/%Y')+1");
// 	$stmt->bindParam(':location', $_POST["location"]);
// 	$stmt->bindParam(':minDate', $range[0]);
// 	$stmt->bindParam(':maxDate', $range[1]);
// 	$name = $_POST["location"];
// }


$stmt->execute();
$logs = $stmt->fetchAll();





$textColour = array( 0, 0, 0 );
$headerColour = array( 100, 100, 100 );
$tableHeaderTopTextColour = array( 255, 255, 255 );
$tableHeaderTopFillColour = array( 25, 91, 38 );
$tableHeaderTopProductTextColour = array( 0, 0, 0 );
$tableHeaderTopProductFillColour = array( 143, 173, 204 );
$tableHeaderLeftTextColour = array( 99, 42, 57 );
$tableHeaderLeftFillColour = array( 184, 207, 229 );
$tableBorderColour = array( 50, 50, 50 );
$tableRowFillColour = array( 213, 170, 170 );
$reportName = "DLSL Exam Schedule System Report for Teaching Load of CBEAM";
$reportNameYPos = 160;
$logoFile = "assets/img/dlslicon.png";
$logoXPos = 7;
$logoYPos = 10;
$logoWidth = 22;
$rowLabels = array( "Q1", "Q2", "Q3", "Q4", "Q5" );
$columnLabels = array( "Section", "Subject Code", "Units", "Faculty Name");
$chartXPos = 20;
$chartYPos = 250;
$chartWidth = 160;
$chartHeight = 180;
$chartXLabel = "Product";
$chartYLabel = "2009 Sales";
$chartYStep = 20000;
$chartColours = array(
array( 255, 100, 100 ),
array( 100, 255, 100 ),
array( 100, 100, 255 ),
array( 255, 255, 100 ),
);
$data = array(
array( 9940, 10100, 9490, 11730 ),
array( 19310, 21140, 20560, 22590 ),
array( 25110, 26260, 25210, 28370 ),
array( 27650, 24550, 30040, 31980 ),
);

$pdf = new FPDF('P','mm','A4');
$pdf->SetTextColor( $textColour[0], $textColour[1], $textColour[2] );


$pdf->AddPage();
$pdf->Image( $logoFile, $logoXPos, $logoYPos, $logoWidth );
// Report Name
$pdf->SetFont( 'Arial', 'B', 15 );
$pdf->Cell( 0, 25, "          ".$reportName, 0, 0, 'S');

$pdf->SetDrawColor( $tableBorderColour[0], $tableBorderColour[1], $tableBorderColour[2] );
$pdf->Ln( 25 );

// Create the table header row
$pdf->SetFont( 'Arial', 'B', 15 );

// "PRODUCT" cell
$pdf->SetTextColor( $tableHeaderTopProductTextColour[0], $tableHeaderTopProductTextColour[1], $tableHeaderTopProductTextColour[2] );
$pdf->SetFillColor( $tableHeaderTopProductFillColour[0], $tableHeaderTopProductFillColour[1], $tableHeaderTopProductFillColour[2] );

// Remaining header cells
// $pdf->SetTextColor( $tableHeaderTopTextColour[0], $tableHeaderTopTextColour[1], $tableHeaderTopTextColour[2] );
$pdf->SetFillColor( $tableHeaderTopFillColour[0], $tableHeaderTopFillColour[1], $tableHeaderTopFillColour[2] );
// for ( $i=0; $i<count($columnLabels); $i++ ) {
//   $pdf->Cell( 30, 12, $columnLabels[$i], 1, 0, 'C', true );
// }
$pdf->Cell( 22, 12, $columnLabels[0], 1, 0, 'C', true );
$pdf->Cell( 36, 12, $columnLabels[1], 1, 0, 'C', true );
$pdf->Cell( 36, 12, $columnLabels[2], 1, 0, 'C', true );
$pdf->Cell( 95, 12, $columnLabels[3], 1, 0, 'C', true );

$pdf->Ln( 23 );
$fill = false;
$row = 0;

foreach ( $logs as $log ) {

  // Create the left header cell
  // $pdf->SetFont( 'Arial', 'B', 15 );
  // $pdf->SetTextColor( $tableHeaderLeftTextColour[0], $tableHeaderLeftTextColour[1], $tableHeaderLeftTextColour[2] );
  // $pdf->SetFillColor( $tableHeaderLeftFillColour[0], $tableHeaderLeftFillColour[1], $tableHeaderLeftFillColour[2] );
  // $pdf->Cell( 46, 12, " " . $rowLabels[$row], 1, 0, 'L', $fill );

  // Create the data cells
  $pdf->SetTextColor( $textColour[0], $textColour[1], $textColour[2] );
  // $pdf->SetFillColor( $tableRowFillColour[0], $tableRowFillColour[1], $tableRowFillColour[2] );
  $pdf->SetFont( 'Arial', '', 13 );

  // for ( $i=0; $i<count($columnLabels); $i++ ) {
  //   $pdf->Cell( 36, 12, ( $log[$i] ), 1, 0, 'C', $fill );
  // }
  	$pdf->SetFont( 'Arial', '', 13 );
	$pdf->Cell( 22, 12, ( $log['section'] ), 1, 0, 'C', $fill );

	$pdf->SetFont( 'Arial', '', 13 );
	$pdf->Cell( 36, 12, ( $log['subjectcode'] ), 1, 0, 'C', $fill );

	$pdf->SetFont( 'Arial', '', 13 );
	$pdf->Cell( 36, 12, ( $log['units'] ), 1, 0, 'C', $fill );

	$pdf->SetFont( 'Arial', '', 13 );
	$pdf->Cell( 95, 12, ( $log['faculty_name'] ), 1, 0, 'C', $fill );


  $row++;
  // $fill = !$fill;
  $pdf->Ln( 12 );
}

// echo "<pre>";
// print_r($logs);

$pdf->Output( "report.pdf", "I" );
?>
