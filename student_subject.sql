-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 04, 2017 at 08:17 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dlslces`
--

-- --------------------------------------------------------

--
-- Table structure for table `student_subject`
--

CREATE TABLE IF NOT EXISTS `student_subject` (
  `user_id` varchar(60) NOT NULL,
  `last_name` varchar(60) NOT NULL,
  `first_name` varchar(60) NOT NULL,
  `college_name` varchar(60) NOT NULL,
  `course` varchar(60) NOT NULL,
  `stud_year` varchar(60) NOT NULL,
  `subjectcode` varchar(60) NOT NULL,
  `units` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_subject`
--

INSERT INTO `student_subject` (`user_id`, `last_name`, `first_name`, `college_name`, `course`, `stud_year`, `subjectcode`, `units`) VALUES
('2012115431', 'ADAJAR', 'FRANCIS GERARD', 'CBEAM', 'BSBA FM', '1st', 'Comski2', '3'),
('2012115431', 'ADAJAR', 'FRANCIS GERARD', 'CBEAM', 'BSBA FM', '1st', 'Finmark', '3'),
('2012115431', 'ADAJAR', 'FRANCIS GERARD', 'CBEAM', 'BSBA FM', '1st', 'Humborg', '3'),
('2012115431', 'ADAJAR', 'FRANCIS GERARD', 'CBEAM', 'BSBA FM', '1st', 'NSTPtwo', '3'),
('2012115431', 'ADAJAR', 'FRANCIS GERARD', 'CBEAM', 'BSBA FM', '1st', 'Physed2', '2'),
('2012115431', 'ADAJAR', 'FRANCIS GERARD', 'CBEAM', 'BSBA FM', '1st', 'Reled12', '3');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
