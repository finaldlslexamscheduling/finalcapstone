-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 08, 2017 at 02:23 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dlslces`
--

-- --------------------------------------------------------

--
-- Structure for view `vw_specialexamrequests`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_specialexamrequests` AS select `student_subject`.`user_id` AS `user_id`,`student_subject`.`first_name` AS `first_name`,`student_subject`.`last_name` AS `last_name`,`student_subject`.`section` AS `section`,`student_subject`.`subjectcode` AS `subjectcode`,`teaching_load_cbeam`.`faculty_name` AS `faculty_name` from (`student_subject` join `teaching_load_cbeam` on(((`student_subject`.`subjectcode` = `teaching_load_cbeam`.`subjectcode`) and (`student_subject`.`section` = `teaching_load_cbeam`.`section`))));

--
-- VIEW  `vw_specialexamrequests`
-- Data: None
--


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
