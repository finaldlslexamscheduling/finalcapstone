<?php
session_start();
include_once 'dbconnect.php';

// if(!isset($_SESSION['user']))
// {
// 	header("Location: index.php");
// }
if(!isset($_POST['changebtn'])){
?>
	<div class="alert alert-danger">
		    <div class="container-fluid">
			  <div class="alert-icon">
				<i class="material-icons">error_outline</i>
			  </div>
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="material-icons">clear</i></span>
			  </button>
		      You need to change the default password and email. 
		    </div>
			</div>
			<?php
}
if ($_SESSION['user_type']=='Secretary'){
  header( 'Location: SecretaryHome.php' ) ;
}
else if ($_SESSION['user_type']=='Student')
{
  header( 'Location: StudentHome.php' ) ;
}
else if ($_SESSION['user_type']=='Dean')
{
  header( 'Location: DeanHome.php' ) ;
}
else if ($_SESSION['user_type']=='Faculty'){
$res=mysql_query("SELECT * FROM users WHERE user_id=".$_SESSION['user']);
$userRow=mysql_fetch_array($res);
$uname = $userRow['user_name'];

if($_POST['changebtn']){

	$email = mysql_real_escape_string($_POST['email']);
	$repeatnewpass = mysql_real_escape_string($_POST['repeatnewpass']);
	$newpass = mysql_real_escape_string($_POST['newpass']);
	$repeatnewpass = mysql_real_escape_string($_POST['repeatnewpass']);

	$email = trim($email);
	$newpass = trim($newpass);
	$repeatnewpass = trim($repeatnewpass);

	$email = $_POST['email'];
	$newpass = $_POST['newpass'];
	$repeatnewpass = $_POST['repeatnewpass'];

	//connect
	require("./dbconnect.php");

	$querypass = mysql_query("SELECT * FROM users WHERE user_id=".$_SESSION['user']);
	$numrowspass = mysql_fetch_array($querypass);
	$dbpass = $numrowspass['user_pass'];

	//make sure the password is correct
	
		if($newpass==$repeatnewpass)
		{
			//update db with new pass
			mysql_query("UPDATE users SET user_pass='$newpass' WHERE user_id=".$_SESSION['user']);
			?>
			<!-- <script>alert('Password has been changed.');</script> -->
			<div class="alert alert-success">
		    <div class="container-fluid">
			  <div class="alert-icon">
				<i class="material-icons">check</i>
			  </div>
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="material-icons">clear</i></span>
			  </button>
		      Successfully Changed.
		    </div>
			</div>
			<?php
		}
		else {
			?>
			<!-- <script>alert('New password and repeat password does not match.');</script> -->
			<div class="alert alert-danger">
		    <div class="container-fluid">
			  <div class="alert-icon">
			    <i class="material-icons">error_outline</i>
			  </div>
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="material-icons">clear</i></span>
			  </button>
		      New Password and Repeat Password does not match.
		    </div>
			</div>
			<?php
		}
	
	
}

?>

<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" href="assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Exam Schedule</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

	<!--     Fonts and icons     -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

	<!-- CSS Files -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/material-kit.css" rel="stylesheet"/>
		<link href="assets/css/demo.css" rel="stylesheet" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.0/jquery.min.js"></script>
<script type="text/javascript">
$(function() 
{
$(".view_comments").click(function() 
{

var ID = $(this).attr("id");

$.ajax({
type: "POST",
url: "viewajax.php",
data: "msg_id="+ ID, 
cache: false,
success: function(html){
$("#view_comments"+ID).prepend(html);
$("#view"+ID).remove();
$("#two_comments"+ID).remove();
}
});

return false;
});
});
</script>

<style type="text/css">
* {
	margin: 0;
	padding: 0;
}

body {
	font-family: "Trebuchet MS", Helvetica, Sans-Serif;
	font-size: 14px;
}

a {
	text-decoration: none;
	color: #ffffff;
}

a:hover {
	color: #000000;
}

#menu {
	position: relative;
	margin-left: 30px;
}

#menu a {
	display: block;
	width: 140px;
}

#menu ul {
	list-style-type: none;
}

#menu li {
	float: left;
	position: relative;
	text-align: center;
}

#menu ul.sub-menu {
	position: absolute;
	left: -10px;
	z-index: 90;
	display:none;
}

#menu ul.sub-menu li {
	text-align: left;
}

#menu li:hover ul.sub-menu {
	display: block;
}
a
{	text-decoration:none; }
	

.egg{
position:relative;
box-shadow: 0 3px 8px rgba(0, 0, 0, 0.25);
background-color:#fff;
border-radius: 3px 3px 3px 3px;
border: 1px solid rgba(100, 100, 100, 0.4);
}
.egg_Body{border-top:1px solid #D1D8E7;color:#808080;}
.egg_Message{font-size:13px !important;font-weight:normal;overflow:hidden}

h3{font-size:13px;color:#333;margin:0;padding:0}
.comment_ui
{
border-bottom:1px solid #e5eaf1;clear:left;float:none;overflow:hidden;padding:6px 4px 3px 6px;width:431px; cursor:pointer;
}
.comment_ui:hover{
background-color: #F7F7F7;
}
.dddd
{
background-color:#f2f2f2;border-bottom:1px solid #e5eaf1;clear:left;float:none;overflow:hidden;margin-bottom:2px;padding:6px 4px 3px 6px;width:431px; 
}
.comment_text{padding:2px 0 4px; color:#333333}
.comment_actual_text{display:inline;padding-left:.4em}

ol { 
	list-style:none;
	margin:0 auto;
	width:500px;
	margin-top: 20px;
}
#mes{
	padding: 0px 3px;

	border-radius: 2px 2px 2px 2px;
	background-color: rgb(240, 61, 37);
	font-size: 9px;
	font-weight: bold;
	color: #fff;
	position: absolute;
	top: 3px;
	left: 110px;
}
.toppointer{
background-image:url(top.png);
    background-position: -82px 0;
    background-repeat: no-repeat;
    height: 11px;
    position: absolute;
    top: -11px;
    width: 20px;
	right: 354px;
}
.clean { display:none}

</style>
</head>

<body class="profile-page">
	<nav class="navbar navbar-transparent navbar-absolute">
    	<div class="container">
        	<!-- Brand and toggle get grouped for better mobile display -->
        	<div class="navbar-header">
        		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example">
            		<span class="sr-only">Toggle navigation</span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
        		</button>
        		<!-- <a class="navbar-brand" href="http://dlslcess.byethost24.com">DLSL CESS</a> -->
						<a href="http://dlslcess.byethost24.com">
						     <div class="logo-container">
						          <div class="logo">
						              <img src="assets/img/dlsl.jpg" alt="DLSL CESS Logo">
						          </div>
						          <div class="brand">
						              &nbsp;&nbsp;Exam Schedule
						          </div>
						      </div>
						</a>
        	</div>

        	<div class="collapse navbar-collapse" id="navigation-example">
        		<ul class="nav navbar-nav navbar-right">
					<li>
    					<a href="FacultyHome.php">
    						Home
    					</a>
    				</li>
    				<li>
    				<div id="menu">
									<ul>
										<li>
											<a href="#" style="padding:10px 0;">
											Notification
											<?php
											include "pdo.php";
											$stmt = $dbh->prepare("SELECT * FROM notifications where approved = '0'");
											$stmt->execute();
											$notifications = $stmt->fetchAll();	
												$stmt = $dbh->prepare("SELECT * FROM specialexamrequest where faculty = :faculty");
											$stmt->bindparam(':faculty', $userRow['first_name']);
											$stmt->execute();
											$facultys = $stmt->fetchAll();



											$number=count($facultys) + count($notifications);	
											


											echo '<span id="mes">'.$number.'</span>';
											?>
											</a>
										<ul class="sub-menu">
											<li class="egg">
											<div class="toppointer"><img src="top.png" /></div>
												<div id="view_comments"></div>

												<div id="two_comments">

												<div class="comment_ui">


												<?php foreach($notifications as $notification){?>
												<div class="comment_text">
												
												<div  class="comment_actual_text"><td>*Exam requires approval in college of <?php echo $notification["college"]?></div>
												</div>
												<?php }?>
												<?php foreach($facultys as $faculty){?>
												<div class="comment_text">
												<div  class="comment_actual_text">*Request for special exam :<?php echo $faculty["userid_name_subject"]?></div>
												</div>
												<?php }?>



												</div>
												
												
												<div class="bbbbbbb" id="view<?php echo $id; ?>">
													<div style="background-color: #a2a3a5; border-bottom-left-radius: 3px; border-bottom-right-radius: 3px; position: relative; z-index: 100; padding:8px; cursor:pointer;">
													<a href="SecretaryScheduleInformation.php" id="<?php echo $id; ?>">View Exam Schedule</a> 
													<a href="FacultySpecialExamRequest.php" id="<?php echo $id; ?>">Special Exam Request</a> 

													</div>
												</div>
											</li>
										</ul>
										</li>
									</ul>
								</div>
							</li>
							<li>
					<a href="FacultyProfile.php">
              Profile
            </a>
            </li>
            <li>
           <li>
						<a href="FacultyProctorSchedule.php">
							Proctor Schedule
						</a>
						</li>
						<li>
						<a href="FacultyExamSchedule.php">
							Exam Schedule
						</a>
						</li>
						<li>
						<a href="FacultySpecialExamRequest.php">
							Special Exam Request
													</a>
						</li>>
            <li>
              <a href="FacultySpecialExamSchedule.php">
                Special Exam Schedule
              </a>
            </li>
						<li>
						<a href="logout.php?logout">Log out</a>
						</a>
						</li>
        		</ul>
        	</div>
    	</div>
    </nav>

    <div class="wrapper">
		<div class="header header-filter" style="background-image: url('assets/img/examples/city.jpg');"></div>

		<div class="main main-raised">
			<div class="profile-content">
	            <div class="container">
	                <div class="row">
	                    <div class="profile">
	                        <div class="avatar">
	                            <img src="assets/img/default-avatar.png" alt="Circle Image" class="img-circle img-responsive img-raised">
	                        </div>
	                        <div class="name">
	                            <h3 class="title">Welcome, <?php echo $userRow['first_name']; ?></h3>
	                        </div>
	                    </div>
	                </div>

					<div class="row">
						<div class="col-md-6 col-md-offset-3">
							<div class="profile-tabs">
								<form class="form" method="post" action="./passchangefaculty.php">
								<div class="content">

									
									<div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">lock_outline</i>
										</span>
										<input type="password" name="newpass" class="form-control" placeholder="New Password" required/>
									</div>

									<div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">lock_outline</i>
										</span>
										<input type="password" name="repeatnewpass" class="form-control" placeholder="Repeat New Password" required/>
									</div>
<div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">lock_outline</i>
										</span>
										<input type="email" name="email" class="form-control" placeholder="Email" required/>
									</div>


									<div class="footer text-center">
										<!-- <a href="#" type="submit" class="btn btn-simple btn-primary btn-lg">Login</a> -->
										<button type="submit" class="btn btn-simple btn-primary btn-lg" name="changebtn" value="Change Password">Submit</button></td>
										<a href="SecretaryProfile.php" class="btn btn-simple btn-primary btn-lg">Back</a>
									</div>

									<!-- If you want to add a checkbox to this form, uncomment this code

									<div class="checkbox">
										<label>
											<input type="checkbox" name="optionsCheckboxes" checked>
											Subscribe to newsletter
										</label>
									</div> -->
								</div>

							</div>
							<!-- End Profile Tabs -->
							<br></br>
						</div>
	        </div>

	            </div>
	        </div>
		</div>

    </div>
    <footer class="footer">
        <div class="container">
            <div class="copyright">
                <center>&copy; 2017 DLSL CESS</center>
            </div>
        </div>
    </footer>


</body>
	<!--   Core JS Files   -->
	<script src="assets/js/jquery.min.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/js/material.min.js"></script>

	<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
	<script src="assets/js/nouislider.min.js" type="text/javascript"></script>

	<!--  Plugin for the Datepicker, full documentation here: http://www.eyecon.ro/bootstrap-datepicker/ -->
	<script src="assets/js/bootstrap-datepicker.js" type="text/javascript"></script>

	<!-- Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc -->
	<script src="assets/js/material-kit.js" type="text/javascript"></script>

</html>

<?php } else {	header( 'Location: index.php' ) ;} ?>