<?php
session_start();

include_once 'dbconnect.php';

if(isset($_SESSION['user'])!="")
{
	header("Location: SecretaryHome.php");
}

if(isset($_POST['btn-login']))
{
	$uname = mysql_real_escape_string($_POST['uname']);
	$upass = mysql_real_escape_string($_POST['pass']);

	// $snum = mysql_real_escape_string($_POST['student_number']);

	$uname = trim($uname);
	$upass = trim($upass);
	// $upass=md5($upass);
	// $password=sha1($upass);
	// $upass=crypt($upass,'st');
	// $snum = trim($snum);

	$res=mysql_query("SELECT * FROM users WHERE user_id='$uname'");
	$row=mysql_fetch_array($res);
	$count = mysql_num_rows($res); // if uname/pass correct it returns must be 1 row
	if($count == 1 && $row['user_pass']== $upass)
	{
		$user_type=$row['user_type'];
		if($user_type == 'Secretary')
		{
			session_start();
			$_SESSION['user'] = $row['user_id'];
			$_SESSION['user_type'] = $row['user_type'];
			header("Location: SecretaryHome.php");
		}
		if($user_type == 'Student')
		{
			session_start();
			$_SESSION['user'] = $row['user_id'];
			$_SESSION['user_type'] = $row['user_type'];
			header("Location: StudentHome.php");
		}
		if($user_type == 'Dean')
		{
			session_start();
			$_SESSION['user'] = $row['user_id'];
			$_SESSION['user_type'] = $row['user_type'];
			header("Location: DeanHome.php");
		}
		if($user_type == 'Faculty')
		{
			session_start();
			$_SESSION['user'] = $row['user_id'];
			$_SESSION['user_type'] = $row['user_type'];
			header("Location: FacultyHome.php");
		}
		if($user_type == 'Department Chair')
		{
			session_start();
			$_SESSION['user'] = $row['user_id'];
			$_SESSION['user_type'] = $row['user_type'];
			header("Location: DepartmentChairHome.php");
		}
	}
	else
	{
		?>
		<div class="alert alert-danger">
			<div class="container-fluid">
			<div class="alert-icon">
				<i class="material-icons">error_outline</i>
			</div>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true"><i class="material-icons">clear</i></span>
			</button>
				Username or Password seems wrong.
			</div>
		</div>
        <?php
	}
}
?>

<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="assets/img/dlslicon.png">
	<!-- <link rel="shortcut icon" href="assets/img/bg.jpg" /> -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Exam Schedule</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

	<!--     Fonts and icons     -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

	<!-- CSS Files -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/material-kit.css" rel="stylesheet"/>
		<link href="assets/css/demo.css" rel="stylesheet" />

</head>

<body class="signup-page">
	<nav class="navbar navbar-transparent navbar-absolute">
    	<div class="container">
        	<!-- Brand and toggle get grouped for better mobile display -->
        	<div class="navbar-header">
        		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example">
            		<span class="sr-only">Toggle navigation</span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
        		</button>
        		<!-- <a class="navbar-brand" href="http://dlslcess.byethost24.com">DLSL CESS</a> -->
						<a href="http://dlslcess.byethost24.com">
						     <div class="logo-container">
						          <div class="logo">
						              <img src="assets/img/dlsl.jpg" alt="DLSL CESS Logo">
						          </div>
						          <div class="brand">
						              &nbsp;&nbsp;Exam Schedule
						          </div>
						      </div>
						</a>
        	</div>

        	<div class="collapse navbar-collapse" id="navigation-example">
        		<ul class="nav navbar-nav navbar-right">
					
        		</ul>
        	</div>
    	</div>
    </nav>

    <div class="wrapper">
		<div class="header header-filter" style="background-image: url('assets/img/examples/aw.jpg'); background-size: cover; background-position: top center;">
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
						<div class="card card-signup">
							<form class="form" method="post" action="">
								<div class="header header-primary text-center">
									<h4>Login</h4>
								</div>
								<div class="content">

									<div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons"></i>
										</span>
										<input type="text" name="uname" class="form-control" placeholder="Username" required/>
									</div>

									<div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons"></i>
										</span>
										<input type="password" name="pass" class="form-control" placeholder="Password" required/>
									</div>

									<!-- If you want to add a checkbox to this form, uncomment this code

									<div class="checkbox">
										<label>
											<input type="checkbox" name="optionsCheckboxes" checked>
											Subscribe to newsletter
										</label>
									</div> -->
								</div>
								<div class="footer text-center">
									<!-- <a href="#" type="submit" class="btn btn-simple btn-primary btn-lg">Login</a> -->
									<button type="submit" class="btn btn-simple btn-primary btn-lg" name="btn-login">Login</button>
									<a href="ForgotPassword.php" class="btn btn-simple btn-primary btn-lg">Forgot Password?</a>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>

			<footer class="footer">
		        <div class="container">
		            <div class="copyright">
		               <center> &copy; 2017<a href="http://dlslcess.byethost24.com" target="_blank"> DLSL CESS</a></center>
		            </div>
		        </div>
		    </footer>

		</div>

    </div>


</body>
	<!--   Core JS Files   -->
	<script src="assets/js/jquery.min.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/js/material.min.js"></script>

	<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
	<script src="assets/js/nouislider.min.js" type="text/javascript"></script>

	<!--  Plugin for the Datepicker, full documentation here: http://www.eyecon.ro/bootstrap-datepicker/ -->
	<script src="assets/js/bootstrap-datepicker.js" type="text/javascript"></script>

	<!-- Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc -->
	<script src="assets/js/material-kit.js" type="text/javascript"></script>

</html>
