
<div class="static-content-wrapper">
<div class="static-content">
<div class="page-content">
<ol class="breadcrumb">


</ol>

<div class="page-heading">            
<h1>Welcome! Admin</h1>

<?php 
$stmt = $dbh->prepare("SELECT * FROM concerns");
$stmt->execute();
$concerns = $stmt->fetchAll();

?>
</div>

<div class="panel-body no-padding">
                <?php if(isset($_GET['Updated'])){?>
                        <button id="notif" type="button" class="btn btn-raised btn-success" data-content="Concern updated!" data-toggle="snackbar" data-timeout="0" style="display:none"></button>
                    <?php } ?>
                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered datatables" id="crudtable">
                        <thead>
                            <tr>
                                <th width="10%">Name</th>
                                <th width="10%">Student Number</th>
                                <th width="25%">Concern</th>
                                <th width="10%">Status</th>
                                <th width="5%">Disable</th>    
                             
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach($concerns as $concern){?>
                            <tr>
                                <td class="text-left"><?php echo $concern['name'];?></td>
                                <td class="text-center"><?php echo $concern['student_number'];?></td>
                                <td class="text-left"><?php echo $concern['concern'];?></td>
                                <td class="text-center"><?php echo ($concern['isDone']?"Done":"Pending");?></td>
                                <td>
                                <a class="btn" href=<?php echo "disableConcern.php?id=".$concern['id']; ?>>Disable</a>
                                </td>
                            </tr>
                        <?php }?>
                        </tbody>


                    </table>
<?php require_once('footer.php'); ?>
 </div>                   
</div>
</div>
</div>
</div>


<?php require_once('script.php'); ?>
<script>
<?php if(isset($_GET['Updated'])){ ?>
                    $(function() {
                        $('#notif').click();
                    });
                <?php } ?>
</script>