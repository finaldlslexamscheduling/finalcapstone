<?php
include 'pdo.php';
date_default_timezone_set('Asia/Singapore');

for($i = 0; $i <2; $i++)
{

	$stmt = $dbh->prepare("SELECT * FROM proctor_con WHERE proctortype = 'PT' ORDER BY proctortype DESC, proctor");
	$stmt->execute();
		$proctors = $stmt->fetchAll();



	foreach($proctors as $proctor)
	{
		if($proctor["proctortype"]=="PT")
		{
			$stmt = $dbh->prepare("SELECT COUNT(*) FROM proctor_con WHERE proctor = :proctor");
			$stmt->bindParam(":proctor",$proctor["proctor"]);
			$stmt->execute();
			$max = $stmt->fetch()[0];

			$stmt = $dbh->prepare("SELECT COUNT(*) FROM exam_schedule WHERE proctor = :proctor");
			$stmt->bindParam(":proctor",$proctor["proctor"]);
			$stmt->execute();
			$curr = $stmt->fetch()[0];

			if($curr==$max)
			{
				continue;
			}
		}
		// $proctor["starttime"] = date('h:i:s A',$proctor["starttime"]);
		// $proctor["endtime"] = date('h:i:s A',$proctor['endtime']);
		$proctor_start = strtotime($proctor["starttime"]);
		$proctor_end = strtotime($proctor["endtime"]);

		if($proctor['availability']=="TTH")
		{
			$dates[0] = "Tuesday";
			$dates[1] = "Thursday";
		}
		else if($proctor['availability']=="MWF")
		{
			$dates[0] = "Monday";
			$dates[1] = "Wednesday";
			$dates[2] = "Friday";
		}
		else if($proctor['availability']=="M")
		{
			$dates[0] = "Monday";
		}
		else if($proctor['availability']=="T")
		{
			$dates[0] = "Tuesday";
		}
		else if($proctor['availability']=="W")
		{
			$dates[0] = "Wednesday";
		}
		else if($proctor['availability']=="TH")
		{
			$dates[0] = "Thursday";
		}
		else if($proctor['availability']=="F")
		{
			$dates[0] = "Friday";
		}
		else if($proctor['availability']=="S")
		{
			$dates[0] = "Saturday";
		}

		$stmt = $dbh->prepare("SELECT * FROM exam_schedule WHERE room_id='5' and taken='1' and proctortaken='0' ORDER BY sub_units DESC");
		$stmt->execute();
		$exam_schedule = $stmt->fetchAll();
		foreach($exam_schedule as $schedule)
		{
			$schedule_start = strtotime($schedule["starttime"]);
			$schedule_end = strtotime($schedule["endtime"]);
			foreach ($dates as $keyDate => $date) {
				if($date == $schedule['day'])
				{
						$stmt = $dbh->prepare("SELECT COUNT(*) FROM exam_schedule WHERE proctor = :proctor AND proctortaken = '1' AND day = :day AND str_to_date(starttime, '%l:%i %p') BETWEEN str_to_date(:starttime, '%l:%i %p') AND str_to_date(:endtime, '%l:%i %p')");
						$stmt->bindParam(':proctor',$proctor['proctor']); 
						$stmt->bindParam(':starttime',$schedule['starttime']);
						$stmt->bindParam(':endtime',$schedule['endtime']);
						$stmt->bindParam(':day',$schedule['day']);
						$stmt->execute();
						$count = $stmt->fetch()[0];
						if($count==0)
						{	

							//echo $proctor["proctor"]."<br>";
							$stmt = $dbh->prepare("UPDATE exam_schedule SET proctor = :proctor, proctortaken='1' WHERE sched_id = :sched_id");
							$stmt->bindParam(':proctor', $proctor['proctor']);
							$stmt->bindParam(':sched_id', $schedule['sched_id']);
							$stmt->execute();
							continue 3;
						}
				}
			}
			
		}

	}



	$stmt = $dbh->prepare("SELECT * FROM proctor_con WHERE proctortype = 'FT' ORDER BY proctortype DESC, proctor");
	$stmt->execute();
	$proctors = $stmt->fetchAll();


	foreach($proctors as $proctor)
	{
		$stmt = $dbh->prepare("SELECT * FROM exam_schedule WHERE room_id='5' and taken='1' and proctortaken='0' ORDER BY sub_units DESC");
		$stmt->execute();
		$exam_schedule = $stmt->fetchAll();
		foreach($exam_schedule as $schedule)
		{
			$schedule_start = strtotime($schedule["starttime"]);
			$schedule_end = strtotime($schedule["endtime"]);
				
			$stmt = $dbh->prepare("SELECT COUNT(*) FROM exam_schedule WHERE proctor = :proctor AND proctortaken = '1' AND day = :day AND str_to_date(starttime, '%l:%i %p') BETWEEN str_to_date(:starttime, '%l:%i %p') AND str_to_date(:endtime, '%l:%i %p')");
			$stmt->bindParam(':proctor',$proctor['proctor']); 
			$stmt->bindParam(':starttime',$schedule['starttime']);
			$stmt->bindParam(':endtime',$schedule['endtime']);
			$stmt->bindParam(':day',$schedule['day']);
			$stmt->execute();
			$count = $stmt->fetch()[0];
			if($count==0)
			{	
				//echo $proctor["proctor"]."<br>";
				$stmt = $dbh->prepare("UPDATE exam_schedule SET proctor = :proctor, proctortaken='1' WHERE sched_id = :sched_id");
				$stmt->bindParam(':proctor', $proctor['proctor']);
				$stmt->bindParam(':sched_id', $schedule['sched_id']);
				$stmt->execute();
			}	
		}

	}
}
$stmt=$dbh->prepare("UPDATE notifications set status='1' where id ='4'");
$stmt->execute();
header("location:SecretaryScheduleInformation.php");

?>