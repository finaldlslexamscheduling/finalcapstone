-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 04, 2016 at 06:35 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cess`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` varchar(20) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  `first_name` varchar(25) NOT NULL,
  `middle_name` varchar(35) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `college_name` enum('CITE', 'Cbeam', 'Ceas', 'Cihtm', 'Con') NOT NULL,
  `user_type` enum('Secretary', 'Student', 'Faculty', 'Dean') NOT NULL,
  `contact_number` varchar(255) NOT NULL

  PRIMARY KEY (`user_id`),
)


--
-- Table structure for table `facultydb`
--

CREATE TABLE IF NOT EXISTS `facultydb` (
  `subject_id` int(10) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `section` varchar(50) NOT NULL,
  `teacher` varchar(100) NOT NULL,
  `exam_type` int(50) NOT NULL,
  `hours` int(10) NOT NULL,
  `mins` int(10) NOT NULL

  PRIMARY KEY (`subject_id`),
  UNIQUE KEY `teacher` (`teacher`)
)

--
-- Dumping data for table `facultydb`
--

INSERT INTO `facultydb` (`subject_id`, `code`, `subject`, `section`, `teacher`, `exam_type`, `hours`, `mins`) VALUES
(1, 'Accfn', 'Accounting and Financials', 'IS2A', 'Amelito Castillo', 'Lec', '1', '0'), (2, 'Systads', 'Systems Administration and Security', 'IT4B', 'Cruzette Ayap', 'Lec', '1', '0'),
(3, 'Caproj2', 'Capstone 2', 'IT4A', 'Mark De Vera', 'Lec', '1', '0'), (4, 'Intele4', 'Information Technology Elective 4', 'IT4B1', 'Agnes Perez', 'Lab', '1', '0')


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
