-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 04, 2017 at 08:27 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dlslces`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` varchar(20) NOT NULL,
  `user_pass` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `user_email` varchar(50) NOT NULL,
  `college_name` enum('Cite','Cbeam','Ceas','Cithm','Con') NOT NULL,
  `user_type` enum('Secretary','Student','Faculty','Dean') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_pass`, `first_name`, `middle_name`, `last_name`, `user_email`, `college_name`, `user_type`) VALUES
('1020304050', 'dlsl123', 'cite', 'cite', 'cite', 'cite@yahoo.com', 'Cite', 'Secretary'),
('1020304051', 'dlsl123', 'cbeam', 'cbeam', 'cbeam', 'cbeam@yahoo.com', 'Cbeam', 'Secretary'),
('1020304052', 'dlsl123', 'ceas', 'ceas', 'ceas', 'ceas@yahoo.com', 'Ceas', 'Secretary'),
('1020304053', 'dlsl123', 'cithm', 'cithm', 'cithm', 'cithm@yahoo.com', 'Cithm', 'Secretary'),
('1020304054', 'dlsl123', 'con', 'con', 'con', 'con@yahoo.com', 'Con', 'Secretary'),
('2012100151', 'dlsl123', 'Darlene Fae', 'S', 'Fabros', '', 'Cite', 'Student'),
('2012115431', 'dlsl123', 'Francis Gerard', '', 'Adajar', '', 'Cbeam', 'Student'),
('2013121471', 'dlsl123', 'G', 'Litan', 'Alcober', '', 'Cite', 'Student'),
('2013121601', 'dlsl123', 'Ara Jobelle', 'Tolentino', 'Tolentino', 'arajtolentino@yahoo.com', 'Cbeam', 'Student'),
('2013122511', 'dlsl123', 'Charisse Mae Angeli', 'A', 'Villones', 'angelvillones@yahoo.com', 'Cbeam', 'Student'),
('2013123101', 'dlsl123', 'Kevin Brad', 'Austria', 'Barbosa', '', 'Cite', 'Student'),
('2013126351', 'dlsl123', 'Carlo Andrew', 'Villanueva', 'Calpe', 'carloandrew.calpe@yahoo.com', 'Cite', 'Student'),
('2013127051', 'dlsl123', 'Lanz Anthony', 'Katigbak', 'Sanchez', '', 'Cite', 'Student'),
('2013135521', 'dlsl123', 'John Paolo', 'D', 'Correa', '', 'Cite', 'Student');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
