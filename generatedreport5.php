 <?php 

include "fpdf/fpdf.php";
include "pdo.php";
session_start();

  date_default_timezone_set("Asia/Manila");
//date_default_timezone_set('Hongkong');
$vd=date("F d Y");
$vsd1=date("F d Y");
$ved1=date("F d Y");
//echo $vd;

class PDF extends FPDF
{

  //Page header
  function Header()
  {

  }

  //Page footer
  function Footer()
  {

  }
}

  // echo "<pre>";
  // print_r($data);


  /////////////////////////////////////////////////////////////////////////export database to pdf format


  //Instanciation of inherited class
  //Instanciation of inherited class
  $pdf=new PDF('L','mm','A4');
  $pdf->AliasNbPages();
  $pdf->AddPage();
  // Adds image to beginning of d

//    $db = mysql_connect('localhost', 'root','');
//    mysql_select_db('dbprint');
    // $query=mysql_query("select * from specialexamrequest");
    
    
    
    $pdf->Ln(10);
    //Image("image name", y, x, image size);
    $pdf->Image("dlsl.jpg",30,25,30);
    $pdf->Ln(20);
    $pdf->SetFont('arial','b',25);
    $pdf->setX(58);$pdf->Cell(0,0,'DLSL',0,0,'L');
    $pdf->Ln(8);
    $pdf->SetFont('arial','b',20);
    $pdf->setX(58);$pdf->Cell(0,0,'Exam Schedule System',0,0,'L');
    
    $pdf->Ln(10);
    $pdf->SetFont('arial','b',10);
    $pdf->setX(58);$pdf->Cell(0,0,'Report for Exam Schedule');
    $pdf->Ln(10);

    //$pdf->Ln(10);
    $pdf->SetFont('arial','b',7);
    $pdf->setX(40);$pdf->Cell(0,0,'Subject Code',0,0,'L');
    $pdf->setX(65);$pdf->Cell(0,0,'Date',0,0,'L');
    $pdf->setX(85);$pdf->Cell(0,0,'Day',0,0,'L');
    $pdf->setX(105);$pdf->Cell(0,0,'Start',0,0,'L');
    $pdf->setX(125);$pdf->Cell(0,0,'End',0,0,'L');
    $pdf->setX(145);$pdf->Cell(0,0,'Section',0,0,'L');
    $pdf->setX(163);$pdf->Cell(0,0,'Room',0,0,'L'); 
    $pdf->setX(190);$pdf->Cell(0,0,'Type',0,0,'L'); 
    $pdf->setX(220);$pdf->Cell(0,0,'Proctor',0,0,'L');
    // $pdf->setX(250);$pdf->Cell(0,0,'College',0,0,'L');
    $pdf->Ln(6.0001);
    // $pdf->setX(20);$pdf->Cell(0,0,'Liquidating',0,0,'L');

    $pdf->setX(7);$pdf->Cell(0,0,'_______________________________________________________________________________________________________________________________________________________________________________________________________________',0,0,'C');
    // while($row=mysql_fetch_array($query))


if($_POST["filter"]=="day")
    $stmt = $dbh->prepare("SELECT * FROM exam_schedule WHERE day = :filter");
  if($_POST["filter"]=="date")
    $stmt = $dbh->prepare("SELECT * FROM exam_schedule WHERE date = :filter");
  if($_POST["filter"]=="starttime")
    $stmt = $dbh->prepare("SELECT * FROM exam_schedule WHERE starttime = :filter");
  if($_POST["filter"]=="endtime")
    $stmt = $dbh->prepare("SELECT * FROM exam_schedule WHERE endtime = :filter");
  if($_POST["filter"]=="room")
    $stmt = $dbh->prepare("SELECT * FROM exam_schedule WHERE room = :filter");
  if($_POST["filter"]=="room_type")
    $stmt = $dbh->prepare("SELECT * FROM exam_schedule WHERE room_type = :filter");
  if($_POST["filter"]=="subject_code")
    $stmt = $dbh->prepare("SELECT * FROM exam_schedule WHERE subject_code = :filter");
  if($_POST["filter"]=="section")
    $stmt = $dbh->prepare("SELECT * FROM exam_schedule WHERE section = :filter");
  if($_POST["filter"]=="proctor")
    $stmt = $dbh->prepare("SELECT * FROM exam_schedule WHERE proctor = :filter");
  // if($_POST["filter"]=="college_name")
  //   $stmt = $dbh->prepare("SELECT * FROM exam_schedule WHERE college_name = :filter");
  
  // if($_POST["filter"]=="day")
  //   $stmt = $dbh->prepare("SELECT * FROM exam_schedule WHERE day = :filter");

  $stmt->bindParam(":filter",$_POST["category_filter"]);
  $stmt->execute();
  $data = $stmt->fetchAll();
    foreach($data as $key => $value){
  
    $pdf->Ln(7);
    
    //$pdf->Image($row['picture'],9.5,8,13);
    
    $pdf->setX(36);$pdf->Cell(0,0,''.$data[$key][1],0,0,'L');
    $pdf->setX(63);$pdf->Cell(0,0,''.$data[$key][2],0,0,'L');
    $pdf->setX(83);$pdf->Cell(0,0,''.$data[$key][4],0,0,'L');
    $pdf->setX(103);$pdf->Cell(0,0,''.$data[$key][5],0,0,'L');
    $pdf->setX(124);$pdf->Cell(0,0,''.$data[$key][7],0,0,'L');
    $pdf->setX(145);$pdf->Cell(0,0,''.$data[$key][8],0,0,'L');
    $pdf->setX(165);$pdf->Cell(0,0,''.$data[$key][11],0,0,'L');
    $pdf->setX(191);$pdf->Cell(0,0,''.$data[$key][12],0,0,'L');
    $pdf->setX(205);$pdf->Cell(0,0,''.$data[$key][15],0,0,'L');
    // $pdf->setX(252);$pdf->Cell(0,0,''.$data[$key][16],0,0,'L');
    }
    $pdf->Ln(7);
    $pdf->setX(7);$pdf->Cell(0,0,' ',0,0,'C');
    $pdf->setX(7);$pdf->Cell(0,0,'_______________________________________________________________________________________________________________________________________________________________________________________________________________',0,0,'C');
    $pdf->Output();
    $pdf->Output('report.pdf','F');
?>