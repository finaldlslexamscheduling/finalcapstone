<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" href="assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>DLSL CESS</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

	<!--     Fonts and icons     -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

	<!-- CSS Files -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/material-kit.css" rel="stylesheet"/>

</head>
<body>

<div class="form-group pull-right">
    <input type="text" class="search form-control" placeholder="What you looking for?">
</div>
<span class="counter pull-right"></span>
<table class="table table-hover table-bordered results">
  <thead>
    <!-- <tr>
      <th>#</th>
      <th class="col-md-5 col-xs-5">Name / Surname</th>
      <th class="col-md-4 col-xs-4">Job</th>
      <th class="col-md-3 col-xs-3">City</th>
    </tr> -->
		<tr>
				<th style="text-align:center;" class="">Availability</th>
				<th style="text-align:center;" class="">Start</th>
				<th style="text-align:center;" class="">End</th>
				<th style="text-align:center;" class="">Proctor</th>
				<th style="text-align:center;" class="">Proctor Type</th>
		</tr>
  </thead>
  <tbody>
    <!-- <tr>
      <th scope="row">1</th>
      <td>Vatanay Özbeyli</td>
      <td>UI & UX</td>
      <td>Istanbul</td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>Burak Özkan</td>
      <td>Software Developer</td>
      <td>Istanbul</td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td>Egemen Özbeyli</td>
      <td>Purchasing</td>
      <td>Kocaeli</td>
    </tr>
    <tr>
      <th scope="row">4</th>
      <td>Engin Kızıl</td>
      <td>Sales</td>
      <td>Bozuyük</td>
    </tr> -->

		<?php
		include_once 'dbconnect.php';
		$college_name=$userRow['college_name'];

			$result = mysql_query("SELECT * FROM proctor_cbeam");
			while($row = mysql_fetch_array($result))
				{
				echo "<tr>";
				echo "<td>" . $row['availability'] . "</td>";
				echo "<td>" . $row['start'] . "</td>";
				echo "<td>" . $row['end'] . "</td>";
				echo "<td>" . $row['proctor'] . "</td>";
				echo "<td>" . $row['proctortype'] . "</td>";
				}

		?>
		</tr>

  </tbody>
</table>

</body>
<!--   Core JS Files   -->
<script src="assets/js/jquery.min.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/material.min.js"></script>

<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="assets/js/nouislider.min.js" type="text/javascript"></script>

<!--  Plugin for the Datepicker, full documentation here: http://www.eyecon.ro/bootstrap-datepicker/ -->
<script src="assets/js/bootstrap-datepicker.js" type="text/javascript"></script>

<!-- Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc -->
<script src="assets/js/material-kit.js" type="text/javascript"></script>

</html>
