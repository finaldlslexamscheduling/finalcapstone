-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 03, 2017 at 08:27 AM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cess`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` varchar(20) NOT NULL,
  `user_pass` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `user_email` varchar(50) NOT NULL,
  `college_name` enum('Cite','Cbeam','Ceas','Cithm','Con') NOT NULL,
  `user_type` enum('Secretary','Dean','Faculty','Student') NOT NULL,
  `contact_number` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_pass`, `first_name`, `middle_name`, `last_name`, `user_email`, `college_name`, `user_type`, `contact_number`) VALUES
('1020304050', 'dlsl123', 'cite', 'cite', 'cite', 'cite@yahoo.com', 'Cite', 'Secretary', '09999999999'),
('1020304051', 'dlsl123', 'cbeam', 'cbeam', 'cbeam', 'cbeam@yahoo.com', 'Cbeam', 'Secretary', '09999999999'),
('1020304052', 'dlsl123', 'ceas', 'ceas', 'ceas', 'ceas@yahoo.com', 'Ceas', 'Secretary', '09999999999'),
('1020304053', 'dlsl123', 'cithm', 'cithm', 'cithm', 'cithm@yahoo.com', 'Cithm', 'Secretary', '09999999999'),
('1020304054', 'dlsl123', 'con', 'con', 'con', 'con@yahoo.com', 'Con', 'Secretary', '09999999999'),
('1020304055', 'dlsl123', 'faculty', 'faculty', 'faculty', 'faculty@yahoo.com', 'Cite', 'Faculty', '09999999999'),
('2013126351', 'dlsl123', 'Carlo Andrew', 'Villanueva', 'Calpe', 'carloandrew.calpe@yahoo.com', 'Cite', 'Student', '09152293144');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
