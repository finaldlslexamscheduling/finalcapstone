<?php
session_start();
include_once 'dbconnect.php';

// if(!isset($_SESSION['user']))
// {
// 	header("Location: index.php");
// }
if ($_SESSION['user_type']=='Secretary'){
  header( 'Location: SecretaryHome.php' ) ;
}
else if ($_SESSION['user_type']=='Student')
{
  header( 'Location: StudentHome.php' ) ;
}
else if ($_SESSION['user_type']=='Dean')
{
  header( 'Location: DeanHome.php' ) ;
}
else if ($_SESSION['user_type']=='Faculty'){
$res=mysql_query("SELECT * FROM users WHERE user_id=".$_SESSION['user']);
$userRow=mysql_fetch_array($res);
?>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" href="assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Exam Schedule</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

	<!--     Fonts and icons     -->
<!-- 	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" /> -->
  <!--   <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

 -->	<!-- CSS Files -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/material-kit.css" rel="stylesheet"/>
		<link href="assets/css/demo.css" rel="stylesheet" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.0/jquery.min.js"></script>
<script type="text/javascript">
$(function() 
{
$(".view_comments").click(function() 
{

var ID = $(this).attr("id");

$.ajax({
type: "POST",
url: "viewajax.php",
data: "msg_id="+ ID, 
cache: false,
success: function(html){
$("#view_comments"+ID).prepend(html);
$("#view"+ID).remove();
$("#two_comments"+ID).remove();
}
});

return false;
});
});
</script>

<style type="text/css">
* {
	margin: 0;
	padding: 0;
}

body {
	font-family: "Trebuchet MS", Helvetica, Sans-Serif;
	font-size: 14px;
}

a {
	text-decoration: none;
	color: #ffffff;
}

a:hover {
	color: #000000;
}

#menu {
	position: relative;
	margin-left: 30px;
}

#menu a {
	display: block;
	width: 140px;
}

#menu ul {
	list-style-type: none;
}

#menu li {
	float: left;
	position: relative;
	text-align: center;
}

#menu ul.sub-menu {
	position: absolute;
	left: -10px;
	z-index: 90;
	display:none;
}

#menu ul.sub-menu li {
	text-align: left;
}

#menu li:hover ul.sub-menu {
	display: block;
}
a
{	text-decoration:none; }
	

.egg{
position:relative;
box-shadow: 0 3px 8px rgba(0, 0, 0, 0.25);
background-color:#fff;
border-radius: 3px 3px 3px 3px;
border: 1px solid rgba(100, 100, 100, 0.4);
}
.egg_Body{border-top:1px solid #D1D8E7;color:#808080;}
.egg_Message{font-size:13px !important;font-weight:normal;overflow:hidden}

h3{font-size:13px;color:#333;margin:0;padding:0}
.comment_ui
{
border-bottom:1px solid #e5eaf1;clear:left;float:none;overflow:hidden;padding:6px 4px 3px 6px;width:431px; cursor:pointer;
}
.comment_ui:hover{
background-color: #F7F7F7;
}
.dddd
{
background-color:#f2f2f2;border-bottom:1px solid #e5eaf1;clear:left;float:none;overflow:hidden;margin-bottom:2px;padding:6px 4px 3px 6px;width:431px; 
}
.comment_text{padding:2px 0 4px; color:#333333}
.comment_actual_text{display:inline;padding-left:.4em}

ol { 
	list-style:none;
	margin:0 auto;
	width:500px;
	margin-top: 20px;
}
#mes{
	padding: 0px 3px;

	border-radius: 2px 2px 2px 2px;
	background-color: rgb(240, 61, 37);
	font-size: 9px;
	font-weight: bold;
	color: #fff;
	position: absolute;
	top: 3px;
	left: 110px;
}
.toppointer{
background-image:url(top.png);
    background-position: -82px 0;
    background-repeat: no-repeat;
    height: 11px;
    position: absolute;
    top: -11px;
    width: 20px;
	right: 354px;
}
.clean { display:none}

</style>

<link href='assets/css/fullcalendar.css' rel='stylesheet' />
<link href='assets/css/fullcalendar.print.css' rel='stylesheet' media='print' />
<script src='assets/js/moment.min.js'></script>
<script src='assets/js/jquery.min.js'></script>
<script src='assets/js/jquery-ui.min.js'></script>
<script src='assets/js/fullcalendar.min.js'></script>
<script>

	$(document).ready(function() {

		var zone = "05:30";  //Change this to your timezone

	$.ajax({
		url: 'process.php',
        type: 'POST', // Send post data
        data: 'type=fetch',
        async: false,
        success: function(s){
        	json_events = s;
        }
	});


	var currentMousePos = {
	    x: -1,
	    y: -1
	};
		jQuery(document).on("mousemove", function (event) {
        currentMousePos.x = event.pageX;
        currentMousePos.y = event.pageY;
    });

		/* initialize the external events
		-----------------------------------------------------------------*/

		$('#external-events .fc-event').each(function() {

			// store data so the calendar knows to render an event upon drop
			$(this).data('event', {
				title: $.trim($(this).text()), // use the element's text as the event title
				stick: true // maintain when user navigates (see docs on the renderEvent method)
			});

			// make the event draggable using jQuery UI
			$(this).draggable({
				zIndex: 999,
				revert: true,      // will cause the event to go back to its
				revertDuration: 0  //  original position after the drag
			});

		});


		/* initialize the calendar
		-----------------------------------------------------------------*/

		$('#calendar').fullCalendar({
			events: JSON.parse(json_events),
			//events: [{"id":"14","title":"New Event","start":"2015-01-24T16:00:00+04:00","allDay":false}],
			utc: true,
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			editable: true,
			droppable: true, 
			slotDuration: '00:30:00',
			eventReceive: function(event){
				var title = event.title;
				var start = event.start.format("YYYY-MM-DD[T]HH:mm:SS");
				$.ajax({
		    		url: 'process.php',
		    		data: 'type=new&title='+title+'&startdate='+start+'&zone='+zone,
		    		type: 'POST',
		    		dataType: 'json',
		    		success: function(response){
		    			event.id = response.eventid;
		    			$('#calendar').fullCalendar('updateEvent',event);
		    		},
		    		error: function(e){
		    			console.log(e.responseText);

		    		}
		    	});
				$('#calendar').fullCalendar('updateEvent',event);
				console.log(event);
			},
			eventDrop: function(event, delta, revertFunc) {
		        var title = event.title;
		        var start = event.start.format();
		        var end = (event.end == null) ? start : event.end.format();
		        $.ajax({
					url: 'process.php',
					data: 'type=resetdate&title='+title+'&start='+start+'&end='+end+'&eventid='+event.id,
					type: 'POST',
					dataType: 'json',
					success: function(response){
						if(response.status != 'success')		    				
						revertFunc();
					},
					error: function(e){		    			
						revertFunc();
						alert('Error processing your request: '+e.responseText);
					}
				});
		    },
		    eventClick: function(event, jsEvent, view) {
		    	console.log(event.id);
		          var title = prompt('Event Title:', event.title, { buttons: { Ok: true, Cancel: false} });
		          if (title){
		              event.title = title;
		              console.log('type=changetitle&title='+title+'&eventid='+event.id);
		              $.ajax({
				    		url: 'process.php',
				    		data: 'type=changetitle&title='+title+'&eventid='+event.id,
				    		type: 'POST',
				    		dataType: 'json',
				    		success: function(response){	
				    			if(response.status == 'success')			    			
		              				$('#calendar').fullCalendar('updateEvent',event);
				    		},
				    		error: function(e){
				    			alert('Error processing your request: '+e.responseText);
				    		}
				    	});
		          }
			},
			eventResize: function(event, delta, revertFunc) {
				console.log(event);
				var title = event.title;
				var end = event.end.format();
				var start = event.start.format();
		        $.ajax({
					url: 'process.php',
					data: 'type=resetdate&title='+title+'&start='+start+'&end='+end+'&eventid='+event.id,
					type: 'POST',
					dataType: 'json',
					success: function(response){
						if(response.status != 'success')		    				
						revertFunc();
					},
					error: function(e){		    			
						revertFunc();
						alert('Error processing your request: '+e.responseText);
					}
				});
		    },
			eventDragStop: function (event, jsEvent, ui, view) {
			    if (isElemOverDiv()) {
			    	var con = confirm('Are you sure to delete this event permanently?');
			    	if(con == true) {
						$.ajax({
				    		url: 'process.php',
				    		data: 'type=remove&eventid='+event.id,
				    		type: 'POST',
				    		dataType: 'json',
				    		success: function(response){
				    			console.log(response);
				    			if(response.status == 'success'){
				    				$('#calendar').fullCalendar('removeEvents');
            						getFreshEvents();
            					}
				    		},
				    		error: function(e){	
				    			alert('Error processing your request: '+e.responseText);
				    		}
			    		});
					}   
				}
			}
		});

	function getFreshEvents(){
		$.ajax({
			url: 'process.php',
	        type: 'POST', // Send post data
	        data: 'type=fetch',
	        async: false,
	        success: function(s){
	        	freshevents = s;
	        }
		});
		$('#calendar').fullCalendar('addEventSource', JSON.parse(freshevents));
	}


	function isElemOverDiv() {
        var trashEl = jQuery('#trash');

        var ofs = trashEl.offset();

        var x1 = ofs.left;
        var x2 = ofs.left + trashEl.outerWidth(true);
        var y1 = ofs.top;
        var y2 = ofs.top + trashEl.outerHeight(true);

        if (currentMousePos.x >= x1 && currentMousePos.x <= x2 &&
            currentMousePos.y >= y1 && currentMousePos.y <= y2) {
            return true;
        }
        return false;
    }

	});

</script>
<style>

	body {
		margin-top: 40px;
		text-align: center;
		font-size: 14px;
		font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
	}

	#trash{
		width:32px;
		height:32px;
		float:left;
		padding-bottom: 15px;
		position: relative;
	}
		
	#wrap {
		width: 1100px;
		margin: 0 auto;
	}
		
	#external-events {
		float: left;
		width: 150px;
		padding: 0 10px;
		border: 1px solid #ccc;
		background: #eee;
		text-align: left;
	}
		
	#external-events h4 {
		font-size: 16px;
		margin-top: 0;
		padding-top: 1em;
	}
		
	#external-events .fc-event {
		margin: 10px 0;
		cursor: pointer;
	}
		
	#external-events p {
		margin: 1.5em 0;
		font-size: 11px;
		color: #666;
	}
		
	#external-events p input {
		margin: 0;
		vertical-align: middle;
	}

	#calendar {
		float: right;
		width: 900px;
	}

</style>
</head>
<body class="profile-page">
	<nav class="navbar navbar-transparent navbar-absolute">
    	<div class="container">
        	<!-- Brand and toggle get grouped for better mobile display -->
        	<div class="navbar-header">
        		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example">
            		<span class="sr-only">Toggle navigation</span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
        		</button>
        		<!-- <a class="navbar-brand" href="http://dlslcess.byethost24.com">DLSL CESS</a> -->
					<!-- 	<a href="http://dlslcess.byethost24.com"> -->
						     <div class="logo-container">
						          <div class="logo">
						              <img src="assets/img/dlsl.jpg" alt="DLSL CESS Logo">
						          </div>
						          <div class="brand">
						              &nbsp;&nbsp;Exam Schedule
						          </div>
						      </div>
						<!-- </a> -->
        	</div>

        	<div class="collapse navbar-collapse" id="navigation-example">
        		<ul class="nav navbar-nav navbar-right">
					<li>
    					<a href="../FacultyHome.php">
    						Home
    					</a>
    				</li>
    				<li>
    				<div id="menu">
									<ul>
										<li>
											<a href="#" style="padding:10px 0;">
											Notification
											<?php
											include "pdo.php";
											$stmt = $dbh->prepare("SELECT * FROM notifications where approved = '1'");
											$stmt->execute();
											$notifications = $stmt->fetchAll();
											$nt=count($notifications);

											if($nt<5)
											{
												$n=0;
											}
											else
											{

												$n=1;
											}
												$stmt = $dbh->prepare("SELECT * FROM specialexamrequest where faculty = :faculty");
											$stmt->bindparam(':faculty', $userRow['first_name']);
											$stmt->execute();
											$facultys = $stmt->fetchAll();



											$number=count($facultys) + $n;	
											


											echo '<span id="mes">'.$number.'</span>';
											?>
											</a>
										<ul class="sub-menu">
											<li class="egg">
											<div class="toppointer"><img src="top.png" /></div>
												<div id="view_comments"></div>

												<div id="two_comments">

												<div class="comment_ui">
<?php 

												if($notif==5)
												{
													?>
												<div class="comment_text">
												<div  class="comment_actual_text">Exam Schedule is Ready for Viewing.</div>
												</div>
												<?php 





												}?>

												<?php foreach($notifications as $notification){?>
												<div class="comment_text">
												
												<div  class="comment_actual_text"><td>*Exam requires approval in college of <?php echo $notification["college"]?></div>
												</div>
												<?php }?>
												<?php foreach($facultys as $faculty){?>
												<div class="comment_text">
												<div  class="comment_actual_text">*Request for special exam :<?php echo $faculty["userid_name_subject"]?></div>
												</div>
												<?php }?>



												</div>
												
												
												<div class="bbbbbbb" id="view<?php echo $id; ?>">
													<div style="background-color: #a2a3a5; border-bottom-left-radius: 3px; border-bottom-right-radius: 3px; position: relative; z-index: 100; padding:8px; cursor:pointer;">
													<a href="SecretaryScheduleInformation.php" id="<?php echo $id; ?>">View Exam Schedule</a> 
													<a href="FacultySpecialExamRequest.php" id="<?php echo $id; ?>">Special Exam Request</a> 

													</div>
												</div>
											</li>
										</ul>
										</li>
									</ul>
								</div>
							</li>
							<li>
					<a href="../FacultyProfile.php">
              Profile
            </a>
            </li>
            <li>
           <li>
						<a href="../FacultyProctorSchedule.php">
							Proctor Schedule
						</a>
						</li>
						<li>
						<a href="../FacultyExamSchedule.php">
							Exam Schedule
						</a>
						</li>
						<li>
						<a href="../FacultySpecialExamRequest.php">
							Special Exam Request
													</a>
						</li>
            <li>
              <a href="../FacultySpecialExamSchedule.php">
                Special Exam Schedule
              </a>
            </li>
						<li>
						<a href="../logout.php?logout">Log out</a>
						</a>
						</li>
        		</ul>
        	</div>
    	</div>
    </nav>

    <div class="wrapper">
		<div class="header header-filter" style="background-image: url('assets/img/examples/m.png');"></div>

		<div class="main main-raised">
			<div class="profile-content">
	            <div class="container">
	                <div class="row">
	                    <div class="profile">
	                        <div class="avatar">
										<!-- <img src="assets/img/default-avatar.png" alt="Circle Image" class="img-circle img-responsive img-raised"> -->
										<?php
												
														
													                    



													                       $p=$userRow['college_name'];


													                       if($p == 'Cbeam')
													                       {
													                       $pc='cbeam.png';

													                       }
													                       elseif($p == 'Cite')
													                       {
													                       $pc='cite.png';

													                       } 
													                       elseif($p == 'Ceas')
													                       {
													                       $pc='ceas.png';

													                       }
													                       elseif($p == 'Cithm')
													                       {
													                       $pc='cithm.png';

													                       }
													                       elseif($p == 'Con')
													                       {
													                       $pc='conursing.png';

													                       }
?>
														
													                       <img src=<?php echo $pc; ?> alt="Circle Image" class="img-circle img-responsive img-raised">
													                      
													                      	
													                  
													                
	                        </div>
	                        <div class="name">
	                            <h3 class="title">Welcome, <?php echo $userRow['first_name']; ?></h3>
	                        </div>
	                    </div>
	                </div>
<h4>*PLEASE INPUT THE ROOM"</h4>
	<div id='wrap'>

		<div id='external-events'>

			<h4>Draggable Events</h4>
					<?php
	    include_once 'pdo.php';
	if ($_SESSION['user_type']=='Faculty'){
 $stmt = $dbh->prepare("SELECT * FROM users WHERE user_id=".$_SESSION['user']);
$stmt->execute();
$users = $stmt->fetchAll();
foreach($users as $user)
{
$fc=$user['first_name'];
}
	    $stmt = $dbh->prepare("SELECT * FROM specialexamrequest where faculty='".$fc."'");
$stmt->execute();
$users = $stmt->fetchAll();
					foreach($users as $user)
{
 echo "<div class='fc-event'>".$user['userid_name_subject']."</div>";




							
				    
										      
					}					            
}











?>

			
			<p>
				<img src="assets/img/trashcan.png" id="trash" alt="">
			</p>
		</div>

		<div id='calendar'></div>

		<div style='clear:both'></div>

		<xspan class="tt">x</xspan>

	</div>
</body>
</html>
<?php
}
?>
