-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 04, 2017 at 11:22 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dlslces`
--

-- --------------------------------------------------------

--
-- Table structure for table `specialexamrequest`
--

CREATE TABLE IF NOT EXISTS `specialexamrequest` (
  `userid_name_subject` varchar(255) NOT NULL,
  `faculty` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `specialexamrequest`
--

INSERT INTO `specialexamrequest` (`userid_name_subject`, `faculty`) VALUES
('2012115431000', 'Gatus, Teodora G., MBA'),
('2012115431000', 'Gatus, Teodora G., MBA'),
('2012115431FRANCIS GERARDADAJARHumborg', 'Magsino, Maria Theresa M., MBA'),
('2012115431FRANCIS GERARDADAJARFinmark', 'Gatus, Teodora G., MBA'),
('2012115431FRANCIS GERARDADAJARFinmark', 'Gatus, Teodora G., MBA'),
('2012115431FRANCIS GERARDADAJARFinmark', 'Gatus, Teodora G., MBA');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
