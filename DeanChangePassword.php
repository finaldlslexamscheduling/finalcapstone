<?php
session_start();
include_once 'dbconnect.php';

// if(!isset($_SESSION['user']))
// {
// 	header("Location: index.php");
// }
if ($_SESSION['user_type']=='Secretary'){
  header( 'Location: SecretaryHome.php' ) ;
}
else if ($_SESSION['user_type']=='Faculty')
{
  header( 'Location: FacultyHome.php' ) ;
}
else if ($_SESSION['user_type']=='Student')
{
  header( 'Location: StudentHome.php' ) ;
}
else if ($_SESSION['user_type']=='Dean'){
$res=mysql_query("SELECT * FROM users WHERE user_id=".$_SESSION['user']);
$userRow=mysql_fetch_array($res);
$uname = $userRow['user_name'];

if($_POST['changebtn']){

	//get the form data
	$oldpass = mysql_real_escape_string($_POST['oldpass']);
	$newpass = mysql_real_escape_string($_POST['newpass']);
	$repeatnewpass = mysql_real_escape_string($_POST['repeatnewpass']);

	$oldpass = trim($oldpass);
	$newpass = trim($newpass);
	$repeatnewpass = trim($repeatnewpass);

	$oldpass = $_POST['oldpass'];
	$newpass = $_POST['newpass'];
	$repeatnewpass = $_POST['repeatnewpass'];

	//connect
	require("./dbconnect.php");

	$querypass = mysql_query("SELECT * FROM users WHERE user_id=".$_SESSION['user']);
	$numrowspass = mysql_fetch_array($querypass);
	$dbpass = $numrowspass['user_pass'];

	//make sure the password is correct
	if($oldpass==$dbpass){
		if($newpass==$repeatnewpass)
		{
			//update db with new pass
			mysql_query("UPDATE users SET user_pass='$newpass' WHERE user_id=".$_SESSION['user']);
			?>
			<!-- <script>alert('Password has been changed.');</script> -->
			<div class="alert alert-success">
		    <div class="container-fluid">
			  <div class="alert-icon">
				<i class="material-icons">check</i>
			  </div>
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="material-icons">clear</i></span>
			  </button>
		      Password has been successfully changed.
		    </div>
			</div>
			<?php
		}
		else {
			?>
			<!-- <script>alert('New password and repeat password does not match.');</script> -->
			<div class="alert alert-danger">
		    <div class="container-fluid">
			  <div class="alert-icon">
			    <i class="material-icons">error_outline</i>
			  </div>
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="material-icons">clear</i></span>
			  </button>
		      New Password and Repeat Password does not match.
		    </div>
			</div>
			<?php
		}
	}
	else{
		?>
	  <!-- <script>alert('Old password does not match.');</script> -->
		<div class="alert alert-danger">
	    <div class="container-fluid">
		  <div class="alert-icon">
		    <i class="material-icons">error_outline</i>
		  </div>
		  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true"><i class="material-icons">clear</i></span>
		  </button>
	      Old Password does not match.
	    </div>
		</div>
	  <?php
	}

}

?>

<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
		<link rel="icon" type="image/png" href="assets/img/favicon.png">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>Exam Schedule</title>
		<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
		<!--     Fonts and icons     -->
		<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
		<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" />
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
		<!-- CSS Files -->
		<link href="assets/css/bootstrap.min.css" rel="stylesheet" />
		<link href="assets/css/material-kit.css" rel="stylesheet"/>
		<link href="assets/css/demo.css" rel="stylesheet" />
		<?php
		include_once 'notification.php';
		?>
	</head>
	<body class="profile-page">
		<nav class="navbar navbar-transparent navbar-absolute">
			<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<!-- <a class="navbar-brand" href="http://dlslcess.byethost24.com">DLSL CESS</a> -->
					<a href="http://dlslcess.byethost24.com">
						<div class="logo-container">
							<div class="logo">
								<img src="assets/img/dlsl.jpg" alt="DLSL CESS Logo">
							</div>
							<div class="brand">
								 &nbsp;&nbsp;Exam Schedule
							</div>
						</div>
					</a>
				</div>
				<div class="collapse navbar-collapse" id="navigation-example">
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a href="DeanHome.php">
								Home
							</a>
						</li>
						<li>
    				<div id="menu">
									<ul>
										<li>
											<a href="#" style="padding:10px 0;">
											Notification
											<?php
											include "pdo.php";

													                       if($college_name == 'Cbeam')
													                       {
													                       $pc='3';

													                       }
													                       elseif($college_name == 'Cite')
													                       {
													                       $pc='1';

													                       } 
													                       elseif($college_name == 'Ceas')
													                       {
													                       $pc='5';

													                       }
													                       elseif($college_name == 'Cithm')
													                       {
													                       $pc='6';

													                       }
													                       elseif($college_name == 'Con')
													                       {
													                       $pc='4';

													                       }
											$stmt = $dbh->prepare("SELECT * FROM notifications where status = '1' and id = :pc and approved='0'");
											$stmt->bindparam(':pc', $pc);
											$stmt->execute();
											$notifications = $stmt->fetchAll();	
											echo '<span id="mes">'.count($notifications).'</span>';
											?>
											</a>
										<ul class="sub-menu">
											<li class="egg">
											<div class="toppointer"><img src="top.png" /></div>
												<div id="view_comments"></div>

												<div id="two_comments">

												<div class="comment_ui">


												<?php foreach($notifications as $notification){?>
												<div class="comment_text">
												<div  class="comment_actual_text">Exam requires approval in college of <?php echo $notification["college"]?></div>
												</div>
												<?php }?>

												</div>
												
												
												<div class="bbbbbbb" id="view<?php echo $id; ?>">
													<div style="background-color: #a2a3a5; border-bottom-left-radius: 3px; border-bottom-right-radius: 3px; position: relative; z-index: 100; padding:8px; cursor:pointer;">
													<a href="#" class="view_comments" id="<?php echo $id; ?>">View all <?php echo $comment_count; ?> comments</a>
													</div>
												</div>
											</li>
										</ul>
										</li>
									</ul>
								</div>
							</li>		
							<li>
								<li>
									<a href="DeanProfile.php">
										Profile
									</a>
								</li>
								<li>
									<a href="DeanScheduleApproval.php">
										Schedule Approval
									</a>
								</li>
								<li>
									<a href="DeanSchedule.php">
										Schedule
									</a>
								</li>
								<li>
									<a href="DeanFacultyInformation.php">
										Faculty Information
									</a>
								</li>
								<li>
									<a href="logout.php?logout">Log out</a>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</nav>
			<div class="wrapper">
				<div class="header header-filter" style="background-image: url('assets/img/examples/m.png');"></div>
				<div class="main main-raised">
					<div class="profile-content">
						<div class="container">
							<div class="row">
								<div class="profile">
									<div class="avatar">
										<!-- <img src="assets/img/default-avatar.png" alt="Circle Image" class="img-circle img-responsive img-raised"> -->
										<?php
													                    $dir  = "./studimages/"; 
													                    $imgs = glob($dir ."*.jpg"); 
													                    

													                    foreach ($imgs AS $i) {
													                      $chopped = chop($i, $_SERVER['PHP_SELF']); 
													                      $chopped2 = substr($chopped, 13);  
													                      $chopped3 = chop($chopped2,".jpg");
													                  	
														
													                       if($chopped3 == $userRow['user_id']){ 
													                       $p=$userRow['college_name'];

													                       if($p == 'Cbeam')
													                       {
													                       $pc='cbeam.png';

													                       }
													                       elseif($p == 'Cite')
													                       {
													                       $pc='cite.png';

													                       } 
													                       elseif($p == 'Ceas')
													                       {
													                       $pc='ceas.png';

													                       }
													                       elseif($p == 'Cithm')
													                       {
													                       $pc='cithm.png';

													                       }
													                       elseif($p == 'Con')
													                       {
													                       $pc='conursing.png';

													                       }
?>
														
													                       <img src=<?php echo $pc; ?> alt="Circle Image" class="img-circle img-responsive img-raised">
													                      <?php }
													                      	
													                    }
													                ?> 
	                        </div>
	                        <div class="name">
	                            <h5 class="title">Welcome <?php echo $userRow['college_name']; ?> Dean</h5>
	                        </div>
	                    </div>
	                </div>
<style>
	.button {
    background-color: #4CAF50; /* Green */
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
    float: right;
}

.button4 {border-radius: 12px;}
</style>

					<div class="row">
						<div class="col-md-6 col-md-offset-3">
							<div class="profile-tabs">

								<form class="form" method="post" action="./DeanChangePassword.php">
								<div class="content">

									<div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">lock_outline</i>
										</span>
										<input type="password" name="oldpass" class="form-control" placeholder="Old Password" required/>
									</div>

									<div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">lock_outline</i>
										</span>
										<input type="password" name="newpass" class="form-control" placeholder="New Password" required/>
									</div>

									<div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">lock_outline</i>
										</span>
										<input type="password" name="repeatnewpass" class="form-control" placeholder="Repeat New Password" required/>
									</div>

									<div class="footer text-center">
										<!-- <a href="#" type="submit" class="btn btn-simple btn-primary btn-lg">Login</a> -->
										<button type="submit" class="btn btn-simple btn-primary btn-lg" name="changebtn" value="Change Password">Change Password</button></td>
										<a href="DeanProfile.php" class="btn btn-simple btn-primary btn-lg">Back</a>
									</div>

								</form>

							</div>
							<!-- End Profile Tabs -->
						</div>
	        </div>

	            </div>
	        </div>
		</div>

    </div>
    <footer class="footer">
        <div class="container">
            <div class="copyright">
                <center>&copy; 2017 DLSL CESS</center>
            </div>
        </div>
    </footer>


</body>
	<!--   Core JS Files   -->
	<script src="assets/js/jquery.min.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/js/material.min.js"></script>

	<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
	<script src="assets/js/nouislider.min.js" type="text/javascript"></script>

	<!--  Plugin for the Datepicker, full documentation here: http://www.eyecon.ro/bootstrap-datepicker/ -->
	<script src="assets/js/bootstrap-datepicker.js" type="text/javascript"></script>

	<!-- Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc -->
	<script src="assets/js/material-kit.js" type="text/javascript"></script>

</html>
<?php } else {	header( 'Location: index.php' ) ;} ?>