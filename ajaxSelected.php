<?php 
	include "pdo.php";
	session_start();

	// if($_POST["type"]=="all")
	// 	$stmt = $dbh->prepare("SELECT * FROM exam_schedule");
	if($_POST["type"]=="NONE")
		$stmt = $dbh->prepare("SELECT * FROM exam_schedule WHERE day = :filter");
	if($_POST["type"]=="day")
		$stmt = $dbh->prepare("SELECT * FROM exam_schedule WHERE day = :filter");
	if($_POST["type"]=="date")
		$stmt = $dbh->prepare("SELECT * FROM exam_schedule WHERE date = :filter");
	if($_POST["type"]=="starttime")
		$stmt = $dbh->prepare("SELECT * FROM exam_schedule WHERE starttime = :filter");
	if($_POST["type"]=="endtime")
		$stmt = $dbh->prepare("SELECT * FROM exam_schedule WHERE endtime = :filter");
	if($_POST["type"]=="room")
		$stmt = $dbh->prepare("SELECT * FROM exam_schedule WHERE room = :filter");
	if($_POST["type"]=="room_type")
		$stmt = $dbh->prepare("SELECT * FROM exam_schedule WHERE room_type = :filter");
	if($_POST["type"]=="subject_code")
		$stmt = $dbh->prepare("SELECT * FROM exam_schedule WHERE subject_code = :filter");
	if($_POST["type"]=="section")
		$stmt = $dbh->prepare("SELECT * FROM exam_schedule WHERE section = :filter");
	if($_POST["type"]=="proctor")
		$stmt = $dbh->prepare("SELECT * FROM exam_schedule WHERE proctor = :filter");
	if($_POST["type"]=="college_name")
		$stmt = $dbh->prepare("SELECT * FROM exam_schedule WHERE college_name = :filter");
	if($_POST["type"]=="noproctor")
		$stmt = $dbh->prepare("SELECT * FROM exam_schedule WHERE taken = 1 and proctortaken = 0");
	if($_POST["type"]=="availableroom")
		$stmt = $dbh->prepare("SELECT * FROM exam_schedule WHERE taken = 0 and room = :filter");

	$stmt->bindParam(":filter",$_POST["selected"]);

	$stmt->execute();
	$data = $stmt->fetchAll();

	// echo "<pre>";
	// print_r($_POST);
	echo json_encode($data);

?>