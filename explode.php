<?php
include 'pdo.php';
$stmt=$dbh->prepare("
ALTER TABLE `exam_schedule` 
CHANGE `sched_id` `sched_id` INT(30) NOT NULL AUTO_INCREMENT, 
CHANGE `date` `date` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, 
CHANGE `day` `day` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, 
CHANGE `time_id` `time_id` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, 
CHANGE `starttime` `starttime` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, 
CHANGE `endtime` `endtime` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, 
CHANGE `units` `units` VARCHAR(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, 
CHANGE `room` `room` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, 
CHANGE `room_type` `room_type` VARCHAR(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, 
CHANGE `room_id` `room_id` VARCHAR(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, 
CHANGE `taken` `taken` TINYINT(1) NOT NULL, 
CHANGE `subject_code` `subject_code` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, 
CHANGE `section` `section` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, 
CHANGE `sub_units` `sub_units` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, 
CHANGE `proctortaken` `proctortaken` TINYINT(1) NOT NULL, 
CHANGE `college_name` `college_name` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, 
CHANGE `proctor` `proctor` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;");
$stmt->execute();
?>
