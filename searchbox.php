<?php
include_once 'dbconnect.php';
 ?>
<!DOCTYPE html>
<html>
<head>
  <link href="../assets/css/material-kit.css" rel="stylesheet"/>
</head>
<body>

<h2>My Customers</h2>

<input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for names.." title="Type in a name">

<table class="table" id="MyTable">
    <thead>
        <tr>
            <th style="text-align:center;" class=""></th>
            <th style="text-align:center;" class="">Code</th>
            <th style="text-align:center;" class="">Subject</th>
            <th style="text-align:center;" class="">Section</th>
            <th style="text-align:center;" class="">Teacher</th>
            <th style="text-align:center;" class="">Exam Type</th>
            <th style="text-align:center;" class="">Hours</th>
            <th style="text-align:center;"  class="">Mins</th>
        </tr>
    </thead>
    <tbody>
      <tr>
        <?php
        include_once 'dbconnect.php';

              $result = mysql_query("SELECT * FROM facultydb");
              while($row = mysql_fetch_array($result))
                {
                echo "<tr>";
                echo "<td>" . $row['subject_id'] . "</td>";
                echo "<td>" . $row['code'] . "</td>";
                echo "<td>" . $row['subject'] . "</td>";
                echo "<td>" . $row['section'] . "</td>";
                echo "<td>" . $row['teacher'] . "</td>";
                echo "<td>" . $row['exam_type'] . "</td>";
                echo "<td>" . $row['hours'] . "</td>";
                echo "<td>" . $row['mins'] . "</td>";
        }
        ?>
      </tr>
    </tbody>
  </table>

<script>
function myFunction() {
  var input, filter, table, tr, td, i;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>

</body>
</html>
