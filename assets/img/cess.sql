-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 26, 2017 at 01:29 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cess`
--

-- --------------------------------------------------------

--
-- Table structure for table `facultyinfo`
--

CREATE TABLE IF NOT EXISTS `facultyinfo` (
  `code` varchar(10) NOT NULL,
  `subject` varchar(50) NOT NULL,
  `section` varchar(6) NOT NULL,
  `teacher` varchar(50) NOT NULL,
  `exam_type` varchar(5) NOT NULL,
  `hours` int(2) NOT NULL,
  `mins` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `facultyinfo`
--

INSERT INTO `facultyinfo` (`code`, `subject`, `section`, `teacher`, `exam_type`, `hours`, `mins`) VALUES
('Accfn', 'Accounting and Financials', 'IS2A', 'Amelito Castillo', 'Lec', 1, 0),
('Systads', 'Systems Administration and Security', 'IT4B', 'Cruzette  Ayap', 'Lec', 1, 0),
('Caproj2', 'Capstone 2', 'IT4A', 'Mark De Vera', 'Lec', 1, 0),
('Intele4', 'Information Technology Elective 4', 'IT4B1', 'Agnes Perez', 'Lab', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `studentdb`
--

CREATE TABLE IF NOT EXISTS `studentdb` (
  `student_number` int(12) NOT NULL,
  `last_name` varchar(25) NOT NULL,
  `first_name` varchar(35) NOT NULL,
  `middle_name` varchar(255) NOT NULL,
  `user_type` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(5) NOT NULL,
  `user_name` varchar(25) NOT NULL,
  `user_email` varchar(35) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  `user_type` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `user_email`, `user_pass`, `user_type`) VALUES
(1, 'admin', 'admin@yahoo.com', 'admin', 'Admin'),
(2, 'secretary', 'secretary@yahoo.com', 'secretary', 'Secretary'),
(3, 'student', 'student@yahoo.com', 'student', 'Student'),
(4, 'dean', 'dean@yahoo.com', 'dean', 'Dean'),
(5, 'faculty', 'faculty@yahoo.com', 'faculty', 'Faculty');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `studentdb`
--
ALTER TABLE `studentdb`
  ADD PRIMARY KEY (`student_number`),
  ADD UNIQUE KEY `user_email` (`user_email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `studentdb`
--
ALTER TABLE `studentdb`
AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
