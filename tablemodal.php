<html>
    <head>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="/script.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    </head>
    <body>
<!--
Data items are displayed in table cells.
Each edit button has data-id="..." attribute to indicate the id of item
-->
<div class="table-responsive">
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Subject_ID</th>
                <th>Code</th>
                <th>Subject</th>
                <th>Section</th>
                <th>Teacher</th>
                <th>Exam Type</th>
                <th>Hours</th>
                <th>Mins</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <tr>
              <?php
              include_once 'dbconnect.php';

              $result = mysql_query("SELECT * FROM facultydb");
              while($row = mysql_fetch_array($result))
                {
                echo "<tr>";
                echo "<td>" . $row['subject_id'] . "</td>";
                echo "<td>" . $row['code'] . "</td>";
                echo "<td>" . $row['subject'] . "</td>";
                echo "<td>" . $row['section'] . "</td>";
                echo "<td>" . $row['teacher'] . "</td>";
                echo "<td>" . $row['exam_type'] . "</td>";
                echo "<td>" . $row['hours'] . "</td>";
                echo "<td>" . $row['mins'] . "</td>";

                  ?>
                <td style="text-align:center;" class="">
                    <button type="button" data-id="<?php $subject_id ?>" class="btn btn-default editButton">Edit</button>
                </td>
              </tr>
              <?php
            }
            ?>
            </tr>
        </tbody>
    </table>
</div>

<!-- The form which is used to populate the item data -->
<form id="userForm"  action="./function.php" method="post" class="form-horizontal" style="display: none;">
    <div class="form-group">
        <label class="col-xs-3 control-label">ID</label>
        <div class="col-xs-3">
            <input type="text" class="form-control" name="subject_id" disabled="disabled" />
        </div>
    </div>

    <div class="form-group">
        <label class="col-xs-3 control-label">Full name</label>
        <div class="col-xs-5">
            <input type="text" class="form-control" name="name" />
        </div>
    </div>

    <div class="form-group">
        <label class="col-xs-3 control-label">Email</label>
        <div class="col-xs-5">
            <input type="text" class="form-control" name="email" />
        </div>
    </div>

    <div class="form-group">
        <label class="col-xs-3 control-label">Website</label>
        <div class="col-xs-5">
            <input type="text" class="form-control" name="website" />
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-5 col-xs-offset-3">
            <button type="submit" class="btn btn-default">Save</button>
        </div>
    </div>
</form>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

</body>
</html>
